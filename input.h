/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_INPUT_H
#define	__YOLK_INPUT_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <util/assert.h>

// Default User Button Configuration
#ifndef	YOLK_INPUT_OK
#define	YOLK_INPUT_OK(in)					YOLK_INPUT_U1(in)
#endif
#ifndef	YOLK_INPUT_CANCEL
#define	YOLK_INPUT_CANCEL(in)				YOLK_INPUT_U2(in)
#endif

// Default Input Delay (cycles)
#ifndef	YOLK_INPUT_DELAY
#define	YOLK_INPUT_DELAY					5
#endif

// Default Sampling Interval
#ifndef	YOLK_INPUT_SAMPLE_INTERVAL
#define	YOLK_INPUT_SAMPLE_INTERVAL			100
#endif

// Generic Input Check
#define	YOLK_INPUT(in, bit)					((in) ? (!(bit & (in)->cstate)) : 0)

// Input - 5-Way Switch
#define	YOLK_INPUT_UP(in)					((in) ? (!((in)->bit_up & (in)->cstate)) : 0)
#define	YOLK_INPUT_DOWN(in)					((in) ? (!((in)->bit_down & (in)->cstate)) : 0)
#define	YOLK_INPUT_LEFT(in)					((in) ? (!((in)->bit_left & (in)->cstate)) : 0)
#define	YOLK_INPUT_RIGHT(in)				((in) ? (!((in)->bit_right & (in)->cstate)) : 0)
#define	YOLK_INPUT_CENTER(in)				((in) ? (!((in)->bit_center & (in)->cstate)) : 0)

// Input - User Buttons
#define	YOLK_INPUT_U1(in)					((in) ? (!((in)->bit_u1 & (in)->cstate)) : 0)
#define	YOLK_INPUT_U2(in)					((in) ? (!((in)->bit_u2 & (in)->cstate)) : 0)

// Input - Numpad
#define	YOLK_INPUT_NUM_0(in)				((in) ? (!((in)->bit_num_0 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_1(in)				((in) ? (!((in)->bit_num_1 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_2(in)				((in) ? (!((in)->bit_num_2 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_3(in)				((in) ? (!((in)->bit_num_3 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_4(in)				((in) ? (!((in)->bit_num_4 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_5(in)				((in) ? (!((in)->bit_num_5 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_6(in)				((in) ? (!((in)->bit_num_6 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_7(in)				((in) ? (!((in)->bit_num_7 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_8(in)				((in) ? (!((in)->bit_num_8 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_9(in)				((in) ? (!((in)->bit_num_9 & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_S(in)				((in) ? (!((in)->bit_num_s & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_H(in)				((in) ? (!((in)->bit_num_h & (in)->cstate)) : 0)
#define	YOLK_INPUT_NUM_KEYS					12

// Special Numbers (None / Star / Hash)
#define	YOLK_INPUT_NONE						0xff
#define	YOLK_INPUT_STAR						0x0a
#define	YOLK_INPUT_HASH						0x0b

// Input Driver Data Size
#define	YOLK_INPUT_DRV_DATA_SIZE			32

// Assert Input Driver Data Size
#define	yolk_input_drv_data_struct(t)		static_assert(sizeof(struct t) <= YOLK_INPUT_DRV_DATA_SIZE, "Yolk Input Driver Data Structure [" #t "] is bigger than YOLK_INPUT_DRV_DATA_SIZE")

// Input Layer Structure
struct yolk_input
{
	// Driver Data
	uint8_t drv_data[YOLK_INPUT_DRV_DATA_SIZE];

	// Driver - Get Raw Input Data
	uint32_t (*drv_get_raw)(struct yolk_input *in);

	// Input Mask
	uint32_t mask;

	// Current State
	uint32_t cstate;

	// Generic Input Bits
	uint32_t bit_up;
	uint32_t bit_down;
	uint32_t bit_left;
	uint32_t bit_right;
	uint32_t bit_center;
	uint32_t bit_u1;
	uint32_t bit_u2;

	// Numpad Input Bits
	uint32_t bit_num_0;
	uint32_t bit_num_1;
	uint32_t bit_num_2;
	uint32_t bit_num_3;
	uint32_t bit_num_4;
	uint32_t bit_num_5;
	uint32_t bit_num_6;
	uint32_t bit_num_7;
	uint32_t bit_num_8;
	uint32_t bit_num_9;
	uint32_t bit_num_s;
	uint32_t bit_num_h;

	// Input Timer
	uint16_t timer;

	// Input Sampling Timer
	uint16_t sample_timer;
};

// Initialize Input
extern void yolk_input_init(struct yolk_input *in, uint32_t mask);

// Set Driver
extern void yolk_input_set_drv(struct yolk_input *in, uint32_t (*drv_get_raw)(struct yolk_input *in));

// Set Generic Input Bits
extern void yolk_input_set_gen_bits(struct yolk_input *in, uint32_t up, uint32_t down, uint32_t left, uint32_t right, uint32_t center, uint32_t u1, uint32_t u2);

// Set Numpad Input Bits
extern void yolk_input_set_numpad_bits(struct yolk_input *in, uint32_t n0, uint32_t n1, uint32_t n2, uint32_t n3, uint32_t n4, uint32_t n5, uint32_t n6, uint32_t n7, uint32_t n8, uint32_t n9, uint32_t star, uint32_t hash);

// Update Input Subsystem
extern void yolk_input_update(struct yolk_input *in);

// Get Clean Input
extern uint32_t yolk_input(struct yolk_input *in);

// Get Number from Input
extern uint8_t yolk_input_get_num(struct yolk_input *in);

#endif
