/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>

// Internal Includes
#include "ui.h"

// UI On
void yolk_ui_on(struct yolk *yk)
{
	// Turn On Display
	gfx_dsp_on(yk->dsp);

	// Turn On Backlight
	if(yk->bklt)													{ yolk_bklt_target_base(yk->bklt); }

	// Enable UI
	yk->ui_enabled = 1;
}

// UI Off
void yolk_ui_off(struct yolk *yk)
{
	// Turn Off Backlight
	if(yk->bklt)
	{
		yolk_bklt_target_zero(yk->bklt);
		while(yk->bklt->cval)										{ yolk_bklt_update(yk->bklt); }
	}

	// Turn Off Display
	gfx_dsp_off(yk->dsp);

	// Disable UI
	yk->ui_enabled = 0;
}

// Re-Enter UI
void yolk_ui_reenter(struct yolk *yk)
{
	// Set Default UI Region
	yolk_ui_set_component_area(yk, 0, gfx_dsp_get_height(yk->dsp));

	// Re-Enter Plugins
	yolk_reenter_plugins(yk);

	// Re-Enter Component
	if(yk->component_reenter)										{ yk->component_reenter(yk, yk->component_data, yk->component_data_size); }

	// Invalidate Entire Display
	yolk_ui_invalidate_all(yk);
}

// Rotate UI Left
void yolk_ui_rotate_left(struct yolk *yk)
{
	uint8_t x;

	// Rotate Display
	gfx_dsp_set_orientation(yk->dsp, (yk->dsp->orientation ? (yk->dsp->orientation - 1) : 3));

	// Check Input
	if(yk->input)
	{
		// Rotate Input
		x = yk->input->bit_up;
		yk->input->bit_up = yk->input->bit_left;
		yk->input->bit_left = yk->input->bit_down;
		yk->input->bit_down = yk->input->bit_right;
		yk->input->bit_right = x;
	}

	// Re-Enter UI
	yolk_ui_reenter(yk);
}

// Rotate UI Right
void yolk_ui_rotate_right(struct yolk *yk)
{
	uint8_t x;

	// Rotate Display
	gfx_dsp_set_orientation(yk->dsp, (yk->dsp->orientation + 1) % 4);

	// Check Input
	if(yk->input)
	{
		// Rotate Input
		x = yk->input->bit_up;
		yk->input->bit_up = yk->input->bit_right;
		yk->input->bit_right = yk->input->bit_down;
		yk->input->bit_down = yk->input->bit_left;
		yk->input->bit_left = x;
	}

	// Re-Enter UI
	yolk_ui_reenter(yk);
}

// Set Component Area
void yolk_ui_set_component_area(struct yolk *yk, uint16_t yoff, uint16_t ysize)
{
	// Set UI Region
	yk->ui_yoff = yoff;
	yk->ui_ysize = ysize;
}

// Set Active Component
void yolk_ui_set_component(struct yolk *yk, yolk_component_cb_t update, yolk_component_cb_t draw, yolk_component_cb_t reenter)
{
	// Set Component
	yk->component_update = update;
	yk->component_draw = draw;
	yk->component_reenter = reenter;
}

// Draw Generic Component Icon & Title
void yolk_ui_draw_component_head(struct yolk *yk, uint16_t icon, char *title, uint16_t title_len)
{
	// Draw Icon
	gfx_tileset_draw(yk->dsp, yolk_icon_tlst(yk), icon, yolk_ui_head_icon_x(yk), yolk_ui_head_icon_y(yk));

	// Draw Title
	gfx_txt_n(yk->dsp, title, title_len, yolk_ui_head_title_x(yk) + 1, yolk_ui_head_title_y(yk) + 1, yolk_ui_head_title_w(yk), 0, yolk_ttl_font(yk), yolk_ttl_col_shadow(yk));
	gfx_txt_n(yk->dsp, title, title_len, yolk_ui_head_title_x(yk), yolk_ui_head_title_y(yk), yolk_ui_head_title_w(yk), 0, yolk_ttl_font(yk), yolk_ttl_col(yk));
}

// Update UI
void yolk_ui_update(struct yolk *yk)
{
	// Check UI Enabled
	if((yk->ui_enabled == 0) || (yk->component_update == 0))		{ return; }

	// Update Active Component
	yk->component_update(yk, yk->component_data, yk->component_data_size);
}

// Draw UI
void yolk_ui_draw(struct yolk *yk)
{
	struct yolk_plugin *p;

	// Check UI Enabled
	if(yk->ui_enabled == 0)											{ return; }

	// Start Drawing Invalidated Frame
	if(gfx_dsp_start_inv(yk->dsp) == 0)
	{
		// Draw Wallpaper
		if(yolk_wp_mode(yk) == YOLK_STYLE_WP_MODE_SOLID)
		{
			if(yolk_wp_col(yk) == 0)								{ gfx_dsp_clr(yk->dsp, 0, 0, gfx_dsp_get_width(yk->dsp), gfx_dsp_get_height(yk->dsp)); }
			else													{ gfx_draw_fill(yk->dsp, 0, 0, gfx_dsp_get_width(yk->dsp), gfx_dsp_get_height(yk->dsp), yolk_wp_col(yk)); }
		}
		else if(yolk_wp_mode(yk) == YOLK_STYLE_WP_MODE_IMG_F)		{ gfx_img_fp_full(yk->dsp, yolk_wp_img_fp(yk), 0, 0); }
		else if(yolk_wp_mode(yk) == YOLK_STYLE_WP_MODE_IMG_P)		{ gfx_img_p_full(yk->dsp, yolk_wp_img_p(yk), 0, 0); }
		else														{ /* NoOp */ }

		// Draw UI Component
		if(yk->component_draw)										{ yk->component_draw(yk, yk->component_data, yk->component_data_size); }

		// Draw Plugins
		p = yk->plugins;
		while(p)													{ if(p->draw) { p->draw(yk, p->user); } p = p->next; }

		// Complete Drawing Invalidated Frame
		gfx_dsp_stop_inv(yk->dsp);
	}
}

// Flush Draw UI (flushed invalidation stack)
void yolk_ui_flush_draw(struct yolk *yk)
{
	// Draw until invalidation stack is empty
	while(yk->dsp->inv_stack.stack_size)							{ yolk_ui_draw(yk); }
}
