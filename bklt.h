/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_BKLT_H
#define	__YOLK_BKLT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Backlight Modes
#define	YOLK_BKLT_MODE_PWM								0
#define	YOLK_BKLT_MODE_DIO								1

// Backlight Structure
struct yolk_bklt
{
	// Mode
	uint8_t mode;

	// Pin
	uint8_t pin;

	// Base Target
	uint8_t base;

	// Speed
	uint8_t speed;

	// Target Value
	uint8_t trgt;

	// Current Value
	uint8_t cval;
};

// Initialize Backlight
extern void yolk_bklt_init(struct yolk_bklt *bklt, uint8_t mode, uint8_t pin, uint8_t base, uint8_t speed);

// Update Backlight
extern void yolk_bklt_update(struct yolk_bklt *bklt);

// Target Base
extern void yolk_bklt_target_base(struct yolk_bklt *bklt);

// Target Zero
extern void yolk_bklt_target_zero(struct yolk_bklt *bklt);

// Set Backlight Target
extern void yolk_bklt_set_target(struct yolk_bklt *bklt, uint8_t target);

// Apply Backlight
extern void yolk_bklt_apply(struct yolk_bklt *bklt);

#endif
