/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>

// Internal Includes
#include "ui.h"
#include "yolk.h"

// Initialize
void yolk_init(struct yolk *yk, struct gfx_dsp *dsp, struct yolk_bklt *bklt, struct yolk_input *input, uint8_t *component_data, uint16_t component_data_size)
{
	// Setup Display, Backlight & Input Layer
	yk->dsp = dsp;
	yk->bklt = bklt;
	yk->input = input;

	// Clear Plugins
	yk->plugins = 0;

	// Set Component Data Buffer
	yk->component_data = component_data;
	yk->component_data_size = component_data_size;

	// Check Display
	if(dsp)
	{
		// Initialize Style
		yolk_style_init(&(yk->style));

		// Set Default UI Region
		yolk_ui_set_component_area(yk, 0, gfx_dsp_get_height(dsp));

		// Enable UI
		yolk_ui_on(yk);
	}

	// Clear Active Componennt
	yolk_ui_set_component(yk, 0, 0, 0);
}

// Attach Plugin
void yolk_attach_plugin(struct yolk *yk, struct yolk_plugin *p, yolk_plugin_cb_t update, yolk_plugin_cb_t draw, yolk_plugin_cb_t reenter, void *user)
{
	// Setup Plugin
	p->update = update;
	p->draw = draw;
	p->reenter = reenter;
	p->user = user;

	// Insert
	p->prev = 0;
	p->next = yk->plugins;
	if(yk->plugins)														{ yk->plugins->prev = p; }
	yk->plugins = p;
}

// Detach Plugin
void yolk_detach_plugin(struct yolk *yk, struct yolk_plugin *p)
{
	// Detach
	if(p->prev)															{ p->prev->next = p->next; }
	else																{ yk->plugins = p->next; }
	if(p->next)															{ p->next->prev = p->prev; }
}

// Re-Enter All Plugins
void yolk_reenter_plugins(struct yolk *yk)
{
	struct yolk_plugin *p;

	// Re-Enter Plugins
	p = yk->plugins;
	while(p)															{ if(p->reenter) { p->reenter(yk, p->user); } p = p->next; }
}

// Update
void yolk_update(struct yolk *yk)
{
	struct yolk_plugin *p;

	// Update Input
	if(yk->input)														{ yolk_input_update(yk->input); }

	// Update Backlight
	if(yk->bklt)														{ yolk_bklt_update(yk->bklt); }

	// Update Plugins
	p = yk->plugins;
	while(p)															{ if(p->update) { p->update(yk, p->user); } p = p->next; }

	// Update UI
	yolk_ui_update(yk);

	// Draw UI
	if(yk->dsp)															{ yolk_ui_draw(yk); }
}
