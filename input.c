/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <stdlib.h>

// Internal Includes
#include "input.h"

// Initialize Input
void yolk_input_init(struct yolk_input *in, uint32_t mask)
{
	// Clear all addresses, bits & timers
	memset(in, 0, sizeof(struct yolk_input));

	// Set Input Mask
	in->mask = mask;
}

// Set Driver
void yolk_input_set_drv(struct yolk_input *in, uint32_t (*drv_get_raw)(struct yolk_input *in))
{
	// Clear Driver Data
	memset(in->drv_data, 0, YOLK_INPUT_DRV_DATA_SIZE);

	// Set Driver
	in->drv_get_raw = drv_get_raw;
}

// Set Generic Input Bits
void yolk_input_set_gen_bits(struct yolk_input *in, uint32_t up, uint32_t down, uint32_t left, uint32_t right, uint32_t center, uint32_t u1, uint32_t u2)
{
	// Set Generic Input Bits
	in->bit_up = up;
	in->bit_down = down;
	in->bit_left = left;
	in->bit_right = right;
	in->bit_center = center;
	in->bit_u1 = u1;
	in->bit_u2 = u2;
}

// Set Numpad Input Bits
void yolk_input_set_numpad_bits(struct yolk_input *in, uint32_t n0, uint32_t n1, uint32_t n2, uint32_t n3, uint32_t n4, uint32_t n5, uint32_t n6, uint32_t n7, uint32_t n8, uint32_t n9, uint32_t star, uint32_t hash)
{
	// Set Numpad Input Bits
	in->bit_num_0 = n0;
	in->bit_num_1 = n1;
	in->bit_num_2 = n2;
	in->bit_num_3 = n3;
	in->bit_num_4 = n4;
	in->bit_num_5 = n5;
	in->bit_num_6 = n6;
	in->bit_num_7 = n7;
	in->bit_num_8 = n8;
	in->bit_num_9 = n9;
	in->bit_num_s = star;
	in->bit_num_h = hash;
}

// Update Input Subsystem
void yolk_input_update(struct yolk_input *in)
{
	// Check Sample Timer
	in->cstate = 0xffffffff;
	if(in->sample_timer < YOLK_INPUT_SAMPLE_INTERVAL)								{ in->sample_timer = in->sample_timer + 1; return; }
	in->sample_timer = 0;

	// Update current state
	in->cstate = yolk_input(in);
}

// Get Clean Input
uint32_t yolk_input(struct yolk_input *in)
{
	uint32_t input;

	// Read Input & Extract Interesting Bits
	input = in->drv_get_raw ? in->drv_get_raw(in) : 0;
	input = (input & in->mask) | ~(in->mask);

	// Check Input Re-trigger Timer { Reset Timer if Input active }
	if(in->timer == YOLK_INPUT_DELAY)												{ if((input & in->mask) != in->mask) { in->timer = 0; } }
	else
	{
		// Inc Timer (advance to end if input inactive)
		if((input & in->mask) == in->mask)											{ in->timer = YOLK_INPUT_DELAY; }
		else																		{ in->timer = in->timer + 1; }

		// Mask Input until Input Timer expires
		input = 0xffffffff;
	}

	return input;
}

// Get Number from Input
uint8_t yolk_input_get_num(struct yolk_input *in)
{
	// Check Input
	if(YOLK_INPUT_NUM_0(in))														{ return 0; }
	else if(YOLK_INPUT_NUM_1(in))													{ return 1; }
	else if(YOLK_INPUT_NUM_2(in))													{ return 2; }
	else if(YOLK_INPUT_NUM_3(in))													{ return 3; }
	else if(YOLK_INPUT_NUM_4(in))													{ return 4; }
	else if(YOLK_INPUT_NUM_5(in))													{ return 5; }
	else if(YOLK_INPUT_NUM_6(in))													{ return 6; }
	else if(YOLK_INPUT_NUM_7(in))													{ return 7; }
	else if(YOLK_INPUT_NUM_8(in))													{ return 8; }
	else if(YOLK_INPUT_NUM_9(in))													{ return 9; }
	else if(YOLK_INPUT_NUM_S(in))													{ return YOLK_INPUT_STAR; }
	else if(YOLK_INPUT_NUM_H(in))													{ return YOLK_INPUT_HASH; }
	else																			{ return YOLK_INPUT_NONE; }
}
