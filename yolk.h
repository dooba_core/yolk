/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_H
#define	__YOLK_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <gfx/dsp.h>

// Internal Includes
#include "bklt.h"
#include "input.h"
#include "icons.h"
#include "style.h"

// Orientation Shortcuts
#define	YOLK_ORIENTATION_TOP									GFX_DSP_ORIENTATION_TOP
#define	YOLK_ORIENTATION_BOTTOM									GFX_DSP_ORIENTATION_BOTTOM
#define	YOLK_ORIENTATION_LEFT									GFX_DSP_ORIENTATION_LEFT
#define	YOLK_ORIENTATION_RIGHT									GFX_DSP_ORIENTATION_RIGHT
#define	yolk_set_orientation(yk, o)								gfx_dsp_set_orientation((yk)->dsp, o)

// Input Shortcuts
#define	yolk_in(yk, bit)										YOLK_INPUT(yk->input, bit)
#define	yolk_in_up(yk)											YOLK_INPUT_UP(yk->input)
#define	yolk_in_down(yk)										YOLK_INPUT_DOWN(yk->input)
#define	yolk_in_left(yk)										YOLK_INPUT_LEFT(yk->input)
#define	yolk_in_right(yk)										YOLK_INPUT_RIGHT(yk->input)
#define	yolk_in_center(yk)										YOLK_INPUT_CENTER(yk->input)
#define	yolk_in_ok(yk)											YOLK_INPUT_OK(yk->input)
#define	yolk_in_cancel(yk)										YOLK_INPUT_CANCEL(yk->input)
#define	yolk_in_u1(yk)											YOLK_INPUT_U1(yk->input)
#define	yolk_in_u2(yk)											YOLK_INPUT_U2(yk->input)
#define	yolk_in_num_0(yk)										YOLK_INPUT_NUM_0(yk->input)
#define	yolk_in_num_1(yk)										YOLK_INPUT_NUM_1(yk->input)
#define	yolk_in_num_2(yk)										YOLK_INPUT_NUM_2(yk->input)
#define	yolk_in_num_3(yk)										YOLK_INPUT_NUM_3(yk->input)
#define	yolk_in_num_4(yk)										YOLK_INPUT_NUM_4(yk->input)
#define	yolk_in_num_5(yk)										YOLK_INPUT_NUM_5(yk->input)
#define	yolk_in_num_6(yk)										YOLK_INPUT_NUM_6(yk->input)
#define	yolk_in_num_7(yk)										YOLK_INPUT_NUM_7(yk->input)
#define	yolk_in_num_8(yk)										YOLK_INPUT_NUM_8(yk->input)
#define	yolk_in_num_9(yk)										YOLK_INPUT_NUM_9(yk->input)
#define	yolk_in_num_star(yk)									YOLK_INPUT_NUM_S(yk->input)
#define	yolk_in_num_hash(yk)									YOLK_INPUT_NUM_H(yk->input)

// Partial Declarations
struct yolk;

// Plugin Callback Type
typedef	void (*yolk_plugin_cb_t)(struct yolk *yk, void *user);

// Component Callback Type
typedef	void (*yolk_component_cb_t)(struct yolk *yk, void *data, uint16_t data_size);

// Plugin Structure
struct yolk_plugin
{
	// Update
	yolk_plugin_cb_t update;

	// Draw
	yolk_plugin_cb_t draw;

	// Re-Enter
	yolk_plugin_cb_t reenter;

	// User Data
	void *user;

	// Linked List
	struct yolk_plugin *next;
	struct yolk_plugin *prev;
};

// Yolk Structure
struct yolk
{
	// Display
	struct gfx_dsp *dsp;

	// Backlight Management
	struct yolk_bklt *bklt;

	// Input Layer
	struct yolk_input *input;

	// Style
	struct yolk_style style;

	// UI Enabled
	uint8_t ui_enabled;

	// UI Region
	uint16_t ui_yoff;
	uint16_t ui_ysize;

	// Plugins
	struct yolk_plugin *plugins;

	// Current Component (Data Buffer and Callbacks)
	uint8_t *component_data;
	uint16_t component_data_size;
	yolk_component_cb_t component_update;
	yolk_component_cb_t component_draw;
	yolk_component_cb_t component_reenter;
};

// Initialize
extern void yolk_init(struct yolk *yk, struct gfx_dsp *dsp, struct yolk_bklt *bklt, struct yolk_input *input, uint8_t *component_data, uint16_t component_data_size);

// Attach Plugin
extern void yolk_attach_plugin(struct yolk *yk, struct yolk_plugin *p, yolk_plugin_cb_t update, yolk_plugin_cb_t draw, yolk_plugin_cb_t reenter, void *user);

// Detach Plugin
extern void yolk_detach_plugin(struct yolk *yk, struct yolk_plugin *p);

// Re-Enter All Plugins
extern void yolk_reenter_plugins(struct yolk *yk);

// Update
extern void yolk_update(struct yolk *yk);

#endif
