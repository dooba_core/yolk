/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_STYLE_H
#define	__YOLK_STYLE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <vfs/vfs.h>
#include <gfx/gfx.h>
#include <gfx/dsp.h>
#include <gfx/col.h>
#include <gfx/txt.h>
#include <gfx/font.h>
#include <gfx/tileset.h>

// Default Assets
#include <gfx/assets/gfx_font_basic6x8.h>
#include <gfx/assets/gfx_font_basic8x16.h>
#include <gfx/assets/gfx_tileset_frame_basic.h>
#include <gfx/assets/gfx_tileset_spinner_basic.h>
#include <gfx/assets/gfx_tileset_pbar_basic.h>
#include "assets/yolk_icon_tileset.h"

// Wallpaper Modes
#define	YOLK_STYLE_WP_MODE_SOLID											0
#define	YOLK_STYLE_WP_MODE_IMG_F											1
#define	YOLK_STYLE_WP_MODE_IMG_P											2

// Default Margins
#define	YOLK_STYLE_DEFAULT_UI_MARGIN										4

// Default Colors
#define	YOLK_STYLE_DEFAULT_TTL_COL											gfx_col(255, 255, 255)
#define	YOLK_STYLE_DEFAULT_TTL_COL_SHADOW									gfx_col(32, 0, 32)
#define	YOLK_STYLE_DEFAULT_TXT_COL											gfx_col(127, 127, 127)
#define	YOLK_STYLE_DEFAULT_SEL_COL											gfx_col(255, 255, 255)
#define	YOLK_STYLE_DEFAULT_CSR_COL											gfx_col(255, 255, 255)
#define	YOLK_STYLE_DEFAULT_CSR_COL_SHADOW									gfx_col(200, 200, 200)
#define	YOLK_STYLE_DEFAULT_WP_COL											gfx_col(0, 0, 0)

// Default Wallapaper
#define	YOLK_STYLE_DEFAULT_WP_MODE											YOLK_STYLE_WP_MODE_SOLID

// Default Assets
#define	YOLK_STYLE_DEFAULT_TTL_FONT											pgm_get_far_address(gfx_font_basic8x16_data)
#define	YOLK_STYLE_DEFAULT_TXT_FONT											pgm_get_far_address(gfx_font_basic6x8_data)
#define	YOLK_STYLE_DEFAULT_FRAME_TLST										pgm_get_far_address(gfx_tileset_frame_basic_data)
#define	YOLK_STYLE_DEFAULT_SPINNER_TLST										pgm_get_far_address(gfx_tileset_spinner_basic_data)
#define	YOLK_STYLE_DEFAULT_PBAR_TLST										pgm_get_far_address(gfx_tileset_pbar_basic_data)
#define	YOLK_STYLE_DEFAULT_ICON_TLST										pgm_get_far_address(yolk_icon_tileset_data)

// Style Structure
struct yolk_style
{
	// Margins
	uint8_t ui_margin;

	// Colors
	uint16_t ttl_col;
	uint16_t ttl_col_shadow;
	uint16_t txt_col;
	uint16_t sel_col;
	uint16_t csr_col;
	uint16_t csr_col_shadow;
	uint16_t wp_col;

	// Wallpaper Mode
	uint8_t wp_mode;

	// Wallpaper Image
	uint8_t wp_img_f_len;
	struct vfs_handle wp_img_fp;
	uint32_t wp_img_p;

	// Fonts
	struct gfx_font ttl_font;
	struct gfx_font txt_font;

	// Tilesets
	struct gfx_tileset frame_tlst;
	struct gfx_tileset spinner_tlst;
	struct gfx_tileset pbar_tlst;
	struct gfx_tileset icon_tlst;
};

// Initialize Style
extern void yolk_style_init(struct yolk_style *s);

// Set Defaults
extern void yolk_style_set_defaults(struct yolk_style *s);

// Set General UI Margin
extern void yolk_style_set_ui_margin(struct yolk_style *s, uint8_t margin);

// Set Title Color
extern void yolk_style_set_ttl_col(struct yolk_style *s, uint16_t col);

// Set Title Shadow Color
extern void yolk_style_set_ttl_shadow_col(struct yolk_style *s, uint16_t col);

// Set Text Color
extern void yolk_style_set_txt_col(struct yolk_style *s, uint16_t col);

// Set Selection Color
extern void yolk_style_set_sel_col(struct yolk_style *s, uint16_t col);

// Set Text Cursor Color
extern void yolk_style_set_csr_col(struct yolk_style *s, uint16_t col);

// Set Text Cursor Shadow Color
extern void yolk_style_set_csr_shadow_col(struct yolk_style *s, uint16_t col);

// Set Wallpaper Color
extern void yolk_style_set_wp_col(struct yolk_style *s, uint16_t col);

// Set Wallpaper Mode
extern void yolk_style_set_wp_mode(struct yolk_style *s, uint8_t mode);

// Set Wallpaper Image - File
extern void yolk_style_set_wp_img_f(struct yolk_style *s, char *path);

// Set Wallpaper Image - File (Fixed Length)
extern void yolk_style_set_wp_img_f_n(struct yolk_style *s, char *path, uint8_t path_len);

// Set Wallpaper Image - Flash
extern void yolk_style_set_wp_img_p(struct yolk_style *s, uint32_t data);

// Set Title Font
extern void yolk_style_set_ttl_font(struct yolk_style *s, uint32_t data);

// Set Text Font
extern void yolk_style_set_txt_font(struct yolk_style *s, uint32_t data);

// Set Frame Tileset
extern void yolk_style_set_frame_tlst(struct yolk_style *s, uint32_t data);

// Set Spinner Tileset
extern void yolk_style_set_spinner_tlst(struct yolk_style *s, uint32_t data);

// Set Progress Bar Tileset
extern void yolk_style_set_pbar_tlst(struct yolk_style *s, uint32_t data);

// Set Icon Tileset
extern void yolk_style_set_icon_tlst(struct yolk_style *s, uint32_t data);

#endif
