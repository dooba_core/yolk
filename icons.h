/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_ICONS_H
#define	__YOLK_ICONS_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Icon Definitions
#define	YOLK_ICON_NONE																						255
#define	YOLK_ICON_NET_NONE																					0
#define	YOLK_ICON_NET_HOME																					1
#define	YOLK_ICON_NET_ROAMING																				2
#define	YOLK_ICON_NET_UNKNOWN																				3
#define	YOLK_ICON_NEW_SMS																					4
#define	YOLK_ICON_SMS																						5
#define	YOLK_ICON_UNREAD																					6
#define	YOLK_ICON_RCVD																						7
#define	YOLK_ICON_SENT																						8
#define	YOLK_ICON_CONTACT																					9
#define	YOLK_ICON_CALL																						10
#define	YOLK_ICON_CALL_IN																					11
#define	YOLK_ICON_CALL_OUT																					12
#define	YOLK_ICON_HANGUP																					13
#define	YOLK_ICON_VIBRATE																					14
#define	YOLK_ICON_SOUND																						15
#define	YOLK_ICON_SYSTEM																					16
#define	YOLK_ICON_MICROSD																					17
#define	YOLK_ICON_MUSIC																						18
#define	YOLK_ICON_PLAY																						19
#define	YOLK_ICON_PAUSE																						20
#define	YOLK_ICON_EDIT																						21
#define	YOLK_ICON_CROSS																						22
#define	YOLK_ICON_NEW																						23
#define	YOLK_ICON_OK																						24
#define	YOLK_ICON_ERROR																						25
#define	YOLK_ICON_FOLDER																					26
#define	YOLK_ICON_FILE																						27
#define	YOLK_ICON_DOOBA																						28
#define	YOLK_ICON_ARROW_L																					29
#define	YOLK_ICON_ARROW_R																					30
#define	YOLK_ICON_BATTERY_HIGH																				31
#define	YOLK_ICON_BATTERY_MED																				32
#define	YOLK_ICON_BATTERY_LOW																				33
#define	YOLK_ICON_BATTERY_CHARGING																			34
#define	YOLK_ICON_COG																						35
#define	YOLK_ICON_COMPUTER																					36
#define	YOLK_ICON_USB																						37
#define	YOLK_ICON_SPIN																						38

#endif
