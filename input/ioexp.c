/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <stdlib.h>
#include <mcp23008/mcp23008.h>

// Internal Includes
#include "ioexp.h"

// Initialize Driver as Yolk Input Layer
void yolk_input_ioexp_init(struct yolk_input *in)
{
	struct yolk_input_ioexp_data *d;

	// Acquire Data
	d = (struct yolk_input_ioexp_data *)(in->drv_data);

	// Set Driver
	yolk_input_set_drv(in, yolk_input_ioexp_get_raw);

	// Set I/O Expanders
	d->addr_count = 0;
}

// Add I/O Expander
void yolk_input_ioexp_add(struct yolk_input *in, uint8_t addr)
{
	struct yolk_input_ioexp_data *d;

	// Acquire Data
	d = (struct yolk_input_ioexp_data *)(in->drv_data);

	// Check Stack Full
	if(d->addr_count >= YOLK_INPUT_IOEXP_MAX)										{ return; }

	// Register Input
	d->addrs[d->addr_count] = addr;
	d->addr_count = d->addr_count + 1;

	// Configure I/O Expander (inputs with internal pull-ups enabled)
	mcp23008_set_dir(addr, 0xff);
	mcp23008_set_pullup(addr, 0xff);
}

// Read Raw Input
uint32_t yolk_input_ioexp_get_raw(struct yolk_input *in)
{
	struct yolk_input_ioexp_data *d;
	uint32_t result = 0;
	uint8_t i;

	// Acquire Data
	d = (struct yolk_input_ioexp_data *)(in->drv_data);

	// Read I/O Multiplexers
	for(i = 0; i < d->addr_count; i = i + 1)										{ result = result | (((uint32_t)mcp23008_get_gpio(d->addrs[i])) << (8 * i)); }

	return result;
}
