/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <stdlib.h>
#include <dio/dio.h>

// Internal Includes
#include "dio.h"

// Initialize Driver as Yolk Input Layer
void yolk_input_dio_init(struct yolk_input *in)
{
	struct yolk_input_dio_data *d;

	// Acquire Data
	d = (struct yolk_input_dio_data *)(in->drv_data);

	// Set Driver
	yolk_input_set_drv(in, yolk_input_dio_get_raw);

	// Set Pins
	d->pin_count = 0;
}

// Add Pin
void yolk_input_dio_add(struct yolk_input *in, uint8_t pin)
{
	struct yolk_input_dio_data *d;

	// Acquire Data
	d = (struct yolk_input_dio_data *)(in->drv_data);

	// Check Stack Full
	if(d->pin_count >= YOLK_INPUT_DIO_MAX)										{ return; }

	// Register Input
	d->pins[d->pin_count] = pin;
	d->pin_count = d->pin_count + 1;

	// Configure DIO (input with internal pull-ups enabled)
	dio_input(pin);
	dio_hi(pin);
}

// Read Raw Input
uint32_t yolk_input_dio_get_raw(struct yolk_input *in)
{
	struct yolk_input_dio_data *d;
	uint32_t result = 0;
	uint8_t i;

	// Acquire Data
	d = (struct yolk_input_dio_data *)(in->drv_data);

	// Read Pins
	for(i = 0; i < d->pin_count; i = i + 1)										{ result = result | (((uint32_t)dio_rd(d->pins[i])) << i); }

	return result;
}
