/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_INPUT_ARD_H
#define	__YOLK_INPUT_ARD_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "../input.h"

// Maximum Pins
#define	YOLK_INPUT_ARD_MAX_PIN						25

// X/Y "virtual" pins
#define	YOLK_INPUT_ARD_PIN_XYMASK					0x0000000f
#define	YOLK_INPUT_ARD_PIN_X1						0
#define	YOLK_INPUT_ARD_PIN_X2						1
#define	YOLK_INPUT_ARD_PIN_Y1						2
#define	YOLK_INPUT_ARD_PIN_Y2						3
#define	YOLK_INPUT_ARD_PIN_DOFF						4

// Ard Input Driver Structure
struct yolk_input_ard_data
{
	// X Axis Input Pin
	uint8_t x_pin;
	uint8_t x_trig_lo;
	uint8_t x_trig_hi;

	// Y Axis Input Pin
	uint8_t y_pin;
	uint8_t y_trig_lo;
	uint8_t y_trig_hi;

	// Pins
	uint8_t pins[YOLK_INPUT_ARD_MAX_PIN];
	uint8_t pin_count;
};

// Define this structure as Yolk Input Driver Data
yolk_input_drv_data_struct(yolk_input_ard_data);

// Initialize Driver as Yolk Input Layer
extern void yolk_input_ard_init(struct yolk_input *in, uint8_t x_pin, uint8_t x_trig_lo, uint8_t x_trig_hi, uint8_t y_pin, uint8_t y_trig_lo, uint8_t y_trig_hi);

// Add Pin
extern void yolk_input_ard_add(struct yolk_input *in, uint8_t pin);

// Read Raw Input
extern uint32_t yolk_input_ard_get_raw(struct yolk_input *in);

#endif
