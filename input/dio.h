/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_INPUT_DIO_H
#define	__YOLK_INPUT_DIO_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "../input.h"

// Maximum Pins
#define	YOLK_INPUT_DIO_MAX						30

// DIO Input Driver Structure
struct yolk_input_dio_data
{
	// Pins
	uint8_t pins[YOLK_INPUT_DIO_MAX];
	uint8_t pin_count;
};

// Define this structure as Yolk Input Driver Data
yolk_input_drv_data_struct(yolk_input_dio_data);

// Initialize Driver as Yolk Input Layer
extern void yolk_input_dio_init(struct yolk_input *in);

// Add Pin
extern void yolk_input_dio_add(struct yolk_input *in, uint8_t pin);

// Read Raw Input
extern uint32_t yolk_input_dio_get_raw(struct yolk_input *in);

#endif
