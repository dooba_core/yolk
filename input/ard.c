/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <stdlib.h>
#include <dio/dio.h>
#include <ard/ard.h>

// Internal Includes
#include "ard.h"

// Initialize Driver as Yolk Input Layer
void yolk_input_ard_init(struct yolk_input *in, uint8_t x_pin, uint8_t x_trig_lo, uint8_t x_trig_hi, uint8_t y_pin, uint8_t y_trig_lo, uint8_t y_trig_hi)
{
	struct yolk_input_ard_data *d;

	// Acquire Data
	d = (struct yolk_input_ard_data *)(in->drv_data);

	// Set Driver
	yolk_input_set_drv(in, yolk_input_ard_get_raw);

	// Set Pins
	d->pin_count = 0;
	d->x_pin = x_pin;
	d->x_trig_lo = x_trig_lo;
	d->x_trig_hi = x_trig_hi;
	d->y_pin = y_pin;
	d->y_trig_lo = y_trig_lo;
	d->y_trig_hi = y_trig_hi;
}

// Add Pin
void yolk_input_ard_add(struct yolk_input *in, uint8_t pin)
{
	struct yolk_input_ard_data *d;

	// Acquire Data
	d = (struct yolk_input_ard_data *)(in->drv_data);

	// Check Stack Full
	if(d->pin_count >= YOLK_INPUT_ARD_MAX_PIN)									{ return; }

	// Register Input
	d->pins[d->pin_count] = pin;
	d->pin_count = d->pin_count + 1;

	// Configure DIO (input with internal pull-ups enabled)
	dio_input(pin);
	dio_hi(pin);
}

// Read Raw Input
uint32_t yolk_input_ard_get_raw(struct yolk_input *in)
{
	struct yolk_input_ard_data *d;
	uint32_t result = 0;
	uint8_t i;
	uint8_t v;

	// Acquire Data
	d = (struct yolk_input_ard_data *)(in->drv_data);

	// Read Pins
	for(i = 0; i < d->pin_count; i = i + 1)										{ result = result | (((uint32_t)dio_rd(d->pins[i])) << (YOLK_INPUT_ARD_PIN_DOFF + i)); }

	// Read Stick
	result = result | YOLK_INPUT_ARD_PIN_XYMASK;
	v = ard(d->x_pin);
	result = result & ~(((uint32_t)(v <= d->x_trig_lo)) << YOLK_INPUT_ARD_PIN_X1);
	result = result & ~(((uint32_t)(v >= d->x_trig_hi)) << YOLK_INPUT_ARD_PIN_X2);
	v = ard(d->y_pin);
	result = result & ~(((uint32_t)(v <= d->y_trig_lo)) << YOLK_INPUT_ARD_PIN_Y1);
	result = result & ~(((uint32_t)(v >= d->y_trig_hi)) << YOLK_INPUT_ARD_PIN_Y2);

	return result;
}
