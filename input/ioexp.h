/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_INPUT_IOEXP_H
#define	__YOLK_INPUT_IOEXP_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "../input.h"

// Maximum I/O Expanders
#define	YOLK_INPUT_IOEXP_MAX						4

// I/O Expander Input Driver Structure
struct yolk_input_ioexp_data
{
	// I/O Expanders
	uint8_t addrs[YOLK_INPUT_IOEXP_MAX];
	uint8_t addr_count;
};

// Define this structure as Yolk Input Driver Data
yolk_input_drv_data_struct(yolk_input_ioexp_data);

// Initialize Driver as Yolk Input Layer
extern void yolk_input_ioexp_init(struct yolk_input *in);

// Add I/O Expander
extern void yolk_input_ioexp_add(struct yolk_input *in, uint8_t addr);

// Read Raw Input
extern uint32_t yolk_input_ioexp_get_raw(struct yolk_input *in);

#endif
