/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>

// Internal Includes
#include "yolk.h"
#include "icons.h"
#include "util/file_icon.h"

// Get Icon by Extension
uint16_t yolk_file_icon(char *name)
{
	// Get Icon
	return yolk_file_icon_n(name, strlen(name));
}

// Get Icon by Extension - Safe
uint16_t yolk_file_icon_n(char *name, uint16_t len)
{
	// Check Name
	if(len > 4)										{ if(strncasecmp(&(name[len - 4]), ".mp3", 4) == 0) { return YOLK_ICON_MUSIC; } }

	// Default Icon
	return YOLK_ICON_FILE;
}
