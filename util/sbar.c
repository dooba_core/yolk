/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>
#include <gfx/dsp.h>
#include <gfx/txt.h>
#include <gfx/pbar.h>
#include <gfx/tileset.h>

// Internal Includes
#include "icons.h"
#include "util/sbar.h"

// Attach Status Bar
void yolk_sbar_attach(struct yolk_sbar *sb, struct yolk *yk, uint8_t position)
{
	// Setup Status Bar
	sb->yk = yk;
	sb->pos = position;

	// Clear Network State
	sb->net_state = YOLK_SBAR_NET_STATE_OFF;

	// Clear Progress Bar
	sb->progress = 0xff;
	sb->progress_icon = YOLK_ICON_NONE;
	sb->progress_timer = 0;

	// Clear Power
	sb->bat_pct = 0xff;
	sb->charging = 0xff;
	sb->pwr_icon = YOLK_ICON_NONE;
	sb->get_bat_pct = 0;
	sb->is_charging = 0;
	sb->pwr_user = 0;
	sb->pwr_update_timer = 0;

	// "Re-Enter" Status Bar
	yolk_sbar_plugin_reenter(yk, sb);

	// Invalidate Entire Sbar
	gfx_dsp_invalidate(yk->dsp, 0, sb->y, gfx_dsp_get_width(yk->dsp), sb->h);

	// Attach Plugin
	yolk_attach_plugin(yk, &(sb->plugin), (yolk_plugin_cb_t)yolk_sbar_plugin_update, (yolk_plugin_cb_t)yolk_sbar_plugin_draw, (yolk_plugin_cb_t)yolk_sbar_plugin_reenter, sb);
}

// Detach Status Bar
void yolk_sbar_detach(struct yolk_sbar *sb)
{
	// Restore Component Area
	yolk_ui_set_component_area(sb->yk, 0, gfx_dsp_get_height(sb->yk->dsp));

	// Detach Plugin
	yolk_detach_plugin(sb->yk, &(sb->plugin));
}

// Set Network State
void yolk_sbar_set_net_state(struct yolk_sbar *sb, uint8_t state)
{
	// Check State
	if(state == sb->net_state)								{ return; }

	// Invalidate Network State Icon
	gfx_dsp_invalidate(sb->yk->dsp, sb->x_icon_net, sb->y_icon, yolk_icon_tlst(sb->yk)->tw, yolk_icon_tlst(sb->yk)->th);
	sb->net_state = state;
}

// Set Progress
void yolk_sbar_set_progress(struct yolk_sbar *sb, uint8_t progress, uint16_t icon)
{
	// Set Progress with default timer
	yolk_sbar_set_progress_t(sb, progress, icon, YOLK_SBAR_PROGRESS_TIMER_DEFAULT);
}

// Set Progress (with timer - in cycles / 0xffff = always on / 0 = off)
void yolk_sbar_set_progress_t(struct yolk_sbar *sb, uint8_t progress, uint16_t icon, uint16_t timer)
{
	// Check Progress
	if(progress != sb->progress)
	{
		// Invalidate Progress Bar
		gfx_dsp_invalidate(sb->yk->dsp, sb->x_pbar, sb->y_pbar, sb->w_pbar, yolk_pbar_tlst(sb->yk)->th);
		sb->progress = progress;
	}

	// Check Icon
	if(icon != sb->progress_icon)
	{
		// Invalidate Progress Icon
		gfx_dsp_invalidate(sb->yk->dsp, sb->x_icon_pbar, sb->y_icon, yolk_icon_tlst(sb->yk)->tw, yolk_icon_tlst(sb->yk)->th);
		sb->progress_icon = icon;
	}

	// Set Timer
	sb->progress_timer = timer;
}

// Set Power State Callbacks
void yolk_sbar_set_pwr_callbacks(struct yolk_sbar *sb, uint8_t (*get_bat_pct)(void *user), uint8_t (*is_charging)(void *user), void *user)
{
	// Set Callbacks
	sb->get_bat_pct = get_bat_pct;
	sb->is_charging = is_charging;

	// Set User Pointer
	sb->pwr_user = user;
}

// Set Power State
void yolk_sbar_set_pwr(struct yolk_sbar *sb, uint8_t bat_pct, uint8_t charging)
{
	uint16_t icon;

	// Check Battery Percentage
	if((bat_pct != sb->bat_pct) || (charging != sb->charging))
	{
		// Invalidate Text
		gfx_dsp_invalidate(sb->yk->dsp, sb->x_text_pwr, sb->y_text, sb->w_text_pwr, yolk_txt_font(sb->yk)->fh);
		sb->bat_pct = bat_pct;
	}

	// Set Charging
	sb->charging = charging;

	// Compute Icon
	if(sb->charging)										{ icon = YOLK_ICON_BATTERY_CHARGING; }
	else
	{
		// Check Level
		if(bat_pct >= YOLK_SBAR_BAT_HIGH)					{ icon = YOLK_ICON_BATTERY_HIGH; }
		else if(bat_pct >= YOLK_SBAR_BAT_MED)				{ icon = YOLK_ICON_BATTERY_MED; }
		else												{ icon = YOLK_ICON_BATTERY_LOW; }
	}

	// Check Icon
	if(icon == sb->pwr_icon)								{ return; }

	// Invalidate Icon
	gfx_dsp_invalidate(sb->yk->dsp, sb->x_icon_pwr, sb->y_icon, yolk_icon_tlst(sb->yk)->tw, yolk_icon_tlst(sb->yk)->tw);

	// Set Icon
	sb->pwr_icon = icon;
}

// Status Bar Plugin Interface - Update
void yolk_sbar_plugin_update(struct yolk *yk, struct yolk_sbar *sb)
{
	uint8_t p;
	uint8_t c;

	// Check Progress Timer
	if((sb->progress_timer) && (sb->progress_timer != 0xffff))
	{
		// Dec Timer
		sb->progress_timer = sb->progress_timer - 1;
		if(sb->progress_timer == 0)
		{
			// End reached - invalidate progress area
			gfx_dsp_invalidate(yk->dsp, sb->x_icon_pbar, sb->y, sb->w_pbar + yolk_icon_tlst(yk)->tw + yolk_ui_margin(yk), sb->h);
			sb->progress_icon = YOLK_ICON_NONE;
			sb->progress = 0xff;
		}
	}

	// Check Power Callbacks
	if((sb->get_bat_pct) || (sb->is_charging))
	{
		// Check Power Update Timer
		sb->pwr_update_timer = sb->pwr_update_timer + 1;
		if(sb->pwr_update_timer >= YOLK_SBAR_PWR_UPDATE_INTERVAL)
		{
			// Update Power Info
			p = sb->get_bat_pct ? sb->get_bat_pct(sb->pwr_user) : 0;
			c = sb->is_charging ? sb->is_charging(sb->pwr_user) : 0;
			yolk_sbar_set_pwr(sb, p, c);

			// Reset Power Update Timer
			sb->pwr_update_timer = 0;
		}
	}
}

// Status Bar Plugin Interface - Draw
void yolk_sbar_plugin_draw(struct yolk *yk, struct yolk_sbar *sb)
{
	uint8_t i;
	uint16_t rx;
	uint16_t rl;

	// Draw Network State
	if(sb->net_state == YOLK_SBAR_NET_STATE_NONE)			{ i = YOLK_ICON_NET_NONE; }
	else if(sb->net_state == YOLK_SBAR_NET_STATE_HOME)		{ i = YOLK_ICON_NET_HOME; }
	else if(sb->net_state == YOLK_SBAR_NET_STATE_ROAMING)	{ i = YOLK_ICON_NET_ROAMING; }
	else if(sb->net_state == YOLK_SBAR_NET_STATE_UNKNOWN)	{ i = YOLK_ICON_NET_UNKNOWN; }
	else													{ i = YOLK_ICON_NONE; }
	if(i != YOLK_ICON_NONE)									{ gfx_tileset_draw(yk->dsp, yolk_icon_tlst(yk), i, sb->x_icon_net, sb->y_icon); }

	// Check Progress Timer
	if(sb->progress_timer)
	{
		// Draw Progress Icon
		if(sb->progress_icon != YOLK_ICON_NONE)				{ gfx_tileset_draw(yk->dsp, yolk_icon_tlst(yk), sb->progress_icon, sb->x_icon_pbar, sb->y_icon); }

		// Draw Progress Bar
		gfx_pbar_draw_default(yk->dsp, yolk_pbar_tlst(yk), sb->progress, sb->w_pbar, sb->x_pbar, sb->y_pbar);
	}

	// Determine Real Power Text Position
	if(sb->charging)										{ rl = 3; }
	else
	{
		// Check Level
		if(sb->bat_pct >= 100)								{ rl = 4; }
		else if(sb->bat_pct >= 10)							{ rl = 3; }
		else												{ rl = 2; }
	}
	rx = sb->x_text_pwr + (yolk_txt_font(yk)->fw * (4 - rl));

	// Draw Power
	if((sb->charging != 0xff) && (sb->bat_pct != 0xff))
	{
		gfx_tileset_draw(yk->dsp, yolk_icon_tlst(yk), sb->pwr_icon, sb->x_icon_pwr, sb->y_icon);
		if(sb->charging)									{ gfx_txt(yk->dsp, "CHG", rx, sb->y_text, 0, 0, yolk_txt_font(yk), yolk_txt_col(yk)); }
		else												{ gfx_txt_f(yk->dsp, rx, sb->y_text, yolk_txt_font(yk)->fw * rl, 0, yolk_txt_font(yk), yolk_txt_col(yk), "%i\\%", sb->bat_pct); }
	}
}

// Status Bar Plugin Interface - Re-Enter
void yolk_sbar_plugin_reenter(struct yolk *yk, struct yolk_sbar *sb)
{
	uint16_t ca_y;
	uint16_t ca_h;

	// Setup Position & Size
	sb->h = (yolk_icon_tlst(yk)->th > yolk_pbar_tlst(yk)->th) ? yolk_icon_tlst(yk)->th : yolk_pbar_tlst(yk)->th;
	sb->h = sb->h + (yolk_ui_margin(yk) * 2);
	sb->y = (sb->pos == YOLK_SBAR_POS_TOP) ? 0 : (gfx_dsp_get_height((yk)->dsp) - sb->h);
	sb->y_icon = sb->y + ((sb->h - yolk_icon_tlst(yk)->th) / 2);
	sb->x_icon_net = yolk_ui_margin(yk);
	sb->x_icon_pbar = yolk_ui_margin(yk) + yolk_icon_tlst(yk)->tw + yolk_ui_margin(yk);
	sb->x_icon_pwr = gfx_dsp_get_width(yk->dsp) - (yolk_icon_tlst(yk)->tw + yolk_ui_margin(yk));
	sb->x_pbar = sb->x_icon_pbar + yolk_icon_tlst(yk)->th + yolk_ui_margin(yk);
	sb->y_pbar = sb->y + ((sb->h - yolk_pbar_tlst(yk)->th) / 2);
	sb->w_pbar = gfx_dsp_get_width(yk->dsp) / 3;
	sb->y_text = sb->y + ((sb->h - yolk_txt_font(yk)->fh) / 2);
	sb->w_text_pwr = yolk_txt_font(yk)->fw * 4;
	sb->x_text_pwr = sb->x_icon_pwr - (sb->w_text_pwr + yolk_ui_margin(yk));

	// Allocate Space from Component Area
	ca_y = (sb->pos == YOLK_SBAR_POS_TOP) ? sb->h : 0;
	ca_h = gfx_dsp_get_height(yk->dsp) - sb->h;
	yolk_ui_set_component_area(yk, ca_y, ca_h);
}
