/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <gfx/dsp.h>

// Internal Includes
#include "ui.h"
#include "util/auto_off.h"

// Attach Auto-Off (with default auto-off time)
void yolk_auto_off_attach(struct yolk_auto_off *aoff, struct yolk *yk)
{
	// Init
	yolk_auto_off_attach_t(aoff, yk, YOLK_AUTO_OFF_DEFAULT_TIME);
}

// Attach Auto-Off with Time
void yolk_auto_off_attach_t(struct yolk_auto_off *aoff, struct yolk *yk, uint32_t time)
{
	// Setup Auto-Off
	aoff->yk = yk;
	aoff->aoff_time = time;
	aoff->timer = 0;

	// Attach Plugin
	yolk_attach_plugin(yk, &(aoff->plugin), (yolk_plugin_cb_t)yolk_auto_off_plugin_update, 0, 0, aoff);
}

// Detach Auto-Off
void yolk_auto_off_detach(struct yolk_auto_off *aoff)
{
	// Detach
	yolk_detach_plugin(aoff->yk, &(aoff->plugin));
}

// Auto-Off Plugin Interface - Update
void yolk_auto_off_plugin_update(struct yolk *yk, struct yolk_auto_off *aoff)
{
	// Check Input Activity
	if(yk->input->cstate == 0xffffffff)
	{
		// No activity - check timer
		if(aoff->timer < aoff->aoff_time)
		{
			// Inc timer
			aoff->timer = aoff->timer + 1;

			// Sleep UI when activity timeout
			if(aoff->timer == aoff->aoff_time)						{ yolk_ui_off(yk); }
		}
	}
	else
	{
		// Check Auto-Off Active { Wake up }
		if(aoff->timer >= aoff->aoff_time)
		{
			yolk_ui_on(yk);
			yk->input->cstate = 0xffffffff;
			yk->input->sample_timer = 0;
			yk->input->timer = 0;
		}

		// Reset Input Acitivity Timer
		aoff->timer = 0;
	}
}
