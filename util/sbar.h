/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_SBAR_H
#define	__YOLK_SBAR_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <vfs/path.h>

// Internal Includes
#include "../yolk.h"
#include "../ui.h"

// Position
#define	YOLK_SBAR_POS_TOP								0
#define	YOLK_SBAR_POS_BOTTOM							1

// Network States
#define	YOLK_SBAR_NET_STATE_OFF							0
#define	YOLK_SBAR_NET_STATE_NONE						1
#define	YOLK_SBAR_NET_STATE_HOME						2
#define	YOLK_SBAR_NET_STATE_ROAMING						3
#define	YOLK_SBAR_NET_STATE_UNKNOWN						4

// Default Progress Timers
#define	YOLK_SBAR_PROGRESS_TIMER_SHORT					5000
#define	YOLK_SBAR_PROGRESS_TIMER_MEDIUM					10000
#define	YOLK_SBAR_PROGRESS_TIMER_LONG					50000
#define	YOLK_SBAR_PROGRESS_TIMER_INFINITE				0xffff
#define	YOLK_SBAR_PROGRESS_TIMER_DEFAULT				YOLK_SBAR_PROGRESS_TIMER_SHORT

// Power Update Interval (Cycles)
#define	YOLK_SBAR_PWR_UPDATE_INTERVAL					50000

// Battery Levels
#define	YOLK_SBAR_BAT_HIGH								70
#define	YOLK_SBAR_BAT_MED								30

// Callback Types
typedef	uint8_t (*yolk_sbar_get_bat_pct_t)(void *user);
typedef	uint8_t (*yolk_sbar_is_charging_t)(void *user);

// Status Bar Structure
struct yolk_sbar
{
	// Yolk
	struct yolk *yk;

	// Yolk Plugin
	struct yolk_plugin plugin;

	// Position
	uint8_t pos;

	// Offsets & Size
	uint16_t y;
	uint16_t h;
	uint16_t y_icon;
	uint16_t x_icon_net;
	uint16_t x_icon_pbar;
	uint16_t x_icon_pwr;
	uint16_t x_pbar;
	uint16_t y_pbar;
	uint16_t w_pbar;
	uint16_t y_text;
	uint16_t x_text_pwr;
	uint16_t w_text_pwr;

	// Network State
	uint8_t net_state;

	// Progress Bar (0xff = off)
	uint8_t progress;
	uint16_t progress_icon;
	uint16_t progress_timer;

	// Battery Percentage (0xff = off)
	uint8_t bat_pct;

	// Charging Status (0xff = off)
	uint8_t charging;

	// Power Icon
	uint16_t pwr_icon;

	// Power Callbacks & User Data
	yolk_sbar_get_bat_pct_t get_bat_pct;
	yolk_sbar_is_charging_t is_charging;
	void *pwr_user;

	// Power Update Timer
	uint16_t pwr_update_timer;
};

// Attach Status Bar
extern void yolk_sbar_attach(struct yolk_sbar *sb, struct yolk *yk, uint8_t position);

// Detach Status Bar
extern void yolk_sbar_detach(struct yolk_sbar *sb);

// Set Network State
extern void yolk_sbar_set_net_state(struct yolk_sbar *sb, uint8_t state);

// Set Progress
extern void yolk_sbar_set_progress(struct yolk_sbar *sb, uint8_t progress, uint16_t icon);

// Set Progress (with timer - in cycles - 0xffff = always on)
extern void yolk_sbar_set_progress_t(struct yolk_sbar *sb, uint8_t progress, uint16_t icon, uint16_t timer);

// Set Power State Callbacks
extern void yolk_sbar_set_pwr_callbacks(struct yolk_sbar *sb, yolk_sbar_get_bat_pct_t get_bat_pct, yolk_sbar_is_charging_t is_charging, void *user);

// Set Power State
extern void yolk_sbar_set_pwr(struct yolk_sbar *sb, uint8_t bat_pct, uint8_t charging);

// Status Bar Plugin Interface - Update
extern void yolk_sbar_plugin_update(struct yolk *yk, struct yolk_sbar *sb);

// Status Bar Plugin Interface - Draw
extern void yolk_sbar_plugin_draw(struct yolk *yk, struct yolk_sbar *sb);

// Status Bar Plugin Interface - Re-Enter
extern void yolk_sbar_plugin_reenter(struct yolk *yk, struct yolk_sbar *sb);

#endif
