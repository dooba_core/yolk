/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <vfs/vfs.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "ui.h"
#include "style.h"
#include "icons.h"
#include "components/menu.h"
#include "util/file_icon.h"
#include "util/file_browser.h"

// File Utilities
struct vfs_handle yolk_file_browser_fdir;
struct vfs_dirent yolk_file_browser_ffis;

// Enter File Browser
uint8_t yolk_file_browser_enter(struct yolk_file_browser *b, struct yolk *yk, char *root, char *ext, yolk_file_browser_event_cb_t event_cb, void *user, uint16_t icon, char *title_fmt, ...)
{
	va_list ap;
	uint8_t r;

	// Acquire Args
	va_start(ap, title_fmt);

	// Enter
	r = yolk_file_browser_enter_n_v(b, yk, root, strlen(root), ext, strlen(ext), event_cb, user, icon, title_fmt, ap);

	// Release Args
	va_end(ap);

	return r;
}

// Enter File Browser - Fixed Length
uint8_t yolk_file_browser_enter_n(struct yolk_file_browser *b, struct yolk *yk, char *root, uint8_t root_len, char *ext, uint8_t ext_len, yolk_file_browser_event_cb_t event_cb, void *user, uint16_t icon, char *title_fmt, ...)
{
	va_list ap;
	uint8_t r;

	// Acquire Args
	va_start(ap, title_fmt);

	// Enter
	r = yolk_file_browser_enter_n_v(b, yk, root, root_len, ext, ext_len, event_cb, user, icon, title_fmt, ap);

	// Release Args
	va_end(ap);

	return r;
}

// Enter File Browser - Fixed Length (variable argument list)
uint8_t yolk_file_browser_enter_n_v(struct yolk_file_browser *b, struct yolk *yk, char *root, uint8_t root_len, char *ext, uint8_t ext_len, yolk_file_browser_event_cb_t event_cb, void *user, uint16_t icon, char *title_fmt, va_list ap)
{
	// Setup File Browser
	b->yk = yk;
	b->event_cb = event_cb;
	b->user = user;
	b->icon = icon;

	// Set Paths
	if(root_len > VFS_PATH_MAXLEN)																					{ return 1; }
	if(root_len)																									{ memcpy(b->path, root, root_len); }
	b->path_len = root_len;
	b->selected_path_len = 0;

	// Set Filter Extension
	if(ext_len > YOLK_FILE_BROWSER_FILTER_EXT_MAXLEN)																{ return 1; }
	if(ext_len)																										{ memcpy(b->filter_ext, ext, ext_len); }
	b->filter_ext_len = ext_len;

	// Set Title
	b->title_len = cvsnprintf(b->title, YOLK_FILE_BROWSER_TITLE_MAXLEN, title_fmt, ap);

	// Browse
	return yolk_file_browser_browse_path(b);
}

// Browse Selected Item
uint8_t yolk_file_browser_browse_selected(struct yolk_file_browser *b)
{
	// Browse Selected Path
	if(b->selected_path_len)																						{ memcpy(b->path, b->selected_path, b->selected_path_len); }
	b->path_len = b->selected_path_len;
	return yolk_file_browser_browse_path(b);
}

// Browse Back
uint8_t yolk_file_browser_browse_back(struct yolk_file_browser *b)
{
	uint8_t i;

	// Check Browse Path Len
	if(b->path_len < 2)																								{ return yolk_file_browser_browse_path(b); }

	// Check Root { Browse Mountpoints }
	if((b->path[b->path_len - 1] == VFS_PATH_DELIM) && (b->path[b->path_len - 2] == VFS_PATH_MOUNTPOINT_DELIM))		{ b->path_len = 0; }
	else
	{
		// Browse Back
		i = b->path_len - 2;
		while((i > 0) && (b->path[i] != VFS_PATH_DELIM))															{ i = i - 1; }
		if(str_chr(b->path, i, VFS_PATH_DELIM) >= i)																{ i = i + 1; }
		b->path_len = i;
	}

	// Browse
	return yolk_file_browser_browse_path(b);
}

// Browse Path
uint8_t yolk_file_browser_browse_path(struct yolk_file_browser *b)
{
	uint8_t i;
	uint16_t icon;

	// Check Path
	if(b->path_len == 0)																							{ return yolk_file_browser_browse_mountpoints(b); }

	// Open Path
	if(vfs_open_n(&yolk_file_browser_fdir, b->path, b->path_len, 0))												{ return 1; }

	// Enter Menu
	yolk_menu_enter(b->yk, (yolk_menu_event_cb_t)yolk_file_browser_menu_event, b, b->icon, "%t", b->title, b->title_len);

	// Run through Directory Items
	i = 0;
	yolk_file_browser_ffis.otype = VFS_OTYPE_FILE;
	while(yolk_file_browser_ffis.otype != VFS_OTYPE_NONE)
	{
		// Read Next Entry
		if(yolk_file_browser_next_browse_item(b))																	{ vfs_close(&yolk_file_browser_fdir); return 1; }

		// Check Type
		if(yolk_file_browser_ffis.otype == VFS_OTYPE_DIR)															{ icon = YOLK_ICON_FOLDER; }
		else if(yolk_file_browser_ffis.otype == VFS_OTYPE_FILE)														{ icon = yolk_file_icon_n(yolk_file_browser_ffis.name, yolk_file_browser_ffis.name_len); }
		else																										{ icon = YOLK_ICON_NONE; }

		// Add Item
		if(icon != YOLK_ICON_NONE)																					{ yolk_menu_add_item(b->yk, i, icon, "%t", yolk_file_browser_ffis.name, yolk_file_browser_ffis.name_len); }

		// Inc Index
		i = i + 1;
	}

	// Close Directory
	vfs_close(&yolk_file_browser_fdir);

	return 0;
}

// Browse Mountpoints
uint8_t yolk_file_browser_browse_mountpoints(struct yolk_file_browser *b)
{
	struct vfs_mountpoint *mp;

	// Enter Menu
	yolk_menu_enter(b->yk, (yolk_menu_event_cb_t)yolk_file_browser_mountpoints_menu_event, b, b->icon, "%t", b->title, b->title_len);

	// Run through Mountpoints
	mp = vfs_mountpoints;
	while(mp)
	{
		// Add Item
		yolk_menu_add_item(b->yk, (uint16_t)mp, YOLK_ICON_SYSTEM, "%t", mp->name, mp->name_len);

		// Next
		mp = mp->next;
	}

	return 0;
}

// Compute Selected Path
uint8_t yolk_file_browser_compute_selected_path(struct yolk_file_browser *b)
{
	// Compute
	return yolk_file_browser_compute_path_n(b, b->path, b->path_len, yolk_menu_selected_item_value(b->yk), b->selected_path, &(b->selected_path_len), &(b->selected_otype));
}

// Compute Path from Index
uint8_t yolk_file_browser_compute_path(struct yolk_file_browser *b, char *path, uint8_t index, char *buf, uint8_t *len, uint8_t *otype)
{
	// Compute
	return yolk_file_browser_compute_path_n(b, path, strlen(path), index, buf, len, otype);
}

// Compute Path from Index - Fixed Length
uint8_t yolk_file_browser_compute_path_n(struct yolk_file_browser *b, char *path, uint8_t path_len, uint8_t index, char *buf, uint8_t *len, uint8_t *otype)
{
	uint8_t i;

	// Open Directory
	if(vfs_open_n(&yolk_file_browser_fdir, path, path_len, 0))														{ return 1; }

	// Run through Directory Items
	i = 0;
	yolk_file_browser_ffis.otype = VFS_OTYPE_FILE;
	while((yolk_file_browser_ffis.otype != VFS_OTYPE_NONE) && (i <= index))
	{
		// Read Next Entry
		if(yolk_file_browser_next_browse_item(b))																	{ vfs_close(&yolk_file_browser_fdir); return 1; }
		i = i + 1;
	}

	// Close Directory
	vfs_close(&yolk_file_browser_fdir);

	// Check Entry
	if(yolk_file_browser_ffis.otype == VFS_OTYPE_NONE)																{ return 2; }

	// Generate Selected Path
	*len = csnprintf(buf, VFS_PATH_MAXLEN, "%t%s%t", path, path_len, ((path[path_len - 1] == VFS_PATH_DELIM) ? "" : VFS_PATH_DELIM_STR), yolk_file_browser_ffis.name, yolk_file_browser_ffis.name_len);
	if(otype)																										{ *otype = yolk_file_browser_ffis.otype; }

	return 0;
}

// Get Next Browse Item
uint8_t yolk_file_browser_next_browse_item(struct yolk_file_browser *b)
{
	uint8_t skip = 1;
	char *n;
	uint8_t nl;
	uint8_t fb;
	char *ext = b->filter_ext;
	uint8_t ext_len = b->filter_ext_len;

	// Find Next Displayable Item (Directory / File)
	while(skip)
	{
		// Read Item, Get Length & First Byte
		if(vfs_readdir(&yolk_file_browser_fdir, &yolk_file_browser_ffis))											{ return 1; }
		n = yolk_file_browser_ffis.name;
		nl = yolk_file_browser_ffis.name_len;
		fb = n[0];

		// Halt on Filter Ext / Dir / End of Dir
		skip = 0;
		if(yolk_file_browser_ffis.otype == VFS_OTYPE_FILE)
		{
			if((fb == '.') || (fb == '_') || (fb < 0x20) || (fb > 0x7e))											{ skip = 1; }
			else
			{
				if(ext_len)
				{
					if(nl < ext_len)																				{ skip = 1; }
					else																							{ if(strncasecmp(&(n[nl - ext_len]), ext, ext_len)) { skip = 1; } }
				}
			}
		}
	}

	return 0;
}

// Mountpoints Menu Event
void yolk_file_browser_mountpoints_menu_event(struct yolk *yk, struct yolk_file_browser *b, uint8_t event)
{
	struct vfs_mountpoint *mp;

	// Check Event
	if(((event == YOLK_MENU_EVENT_CANCEL) || (event == YOLK_MENU_EVENT_BACK)) && (b->event_cb))						{ b->event_cb(b, yk, b->user, event); return; }
	if((event != YOLK_MENU_EVENT_SELECT) && (event != YOLK_MENU_EVENT_RIGHT))										{ return; }

	// Acquire Mountpoint
	mp = (struct vfs_mountpoint *)(uint16_t)(yolk_menu_selected_item_value(yk));

	// Prepare Root
	b->path_len = csnprintf(b->path, VFS_PATH_MAXLEN, "%t%c%c", mp->name, mp->name_len, VFS_PATH_MOUNTPOINT_DELIM, VFS_PATH_DELIM);
	b->selected_path_len = 0;

	// Go
	yolk_file_browser_browse_path(b);
}

// Browse Menu Event
void yolk_file_browser_menu_event(struct yolk *yk, struct yolk_file_browser *b, uint8_t event)
{
	// Construct Selected Path
	yolk_file_browser_compute_selected_path(b);

	// Forward Event down the line
	if(b->event_cb)																									{ b->event_cb(b, yk, b->user, event); }
}
