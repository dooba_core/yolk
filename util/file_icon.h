/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_FILE_ICON_H
#define	__YOLK_FILE_ICON_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Get Icon by Extension
extern uint16_t yolk_file_icon(char *name);

// Get Icon by Extension - Safe
extern uint16_t yolk_file_icon_n(char *name, uint16_t len);

#endif
