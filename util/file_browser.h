/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_FILE_BROWSER_H
#define	__YOLK_FILE_BROWSER_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <vfs/path.h>

// Internal Includes
#include "../yolk.h"

// Filter Extension Max Length
#define	YOLK_FILE_BROWSER_FILTER_EXT_MAXLEN		5

// Title Max Length
#define	YOLK_FILE_BROWSER_TITLE_MAXLEN			20

// Event Callback Type
struct yolk_file_browser;
typedef	void (*yolk_file_browser_event_cb_t)(struct yolk_file_browser *b, struct yolk *yk, void *user, uint8_t event);

// File Browser Structure
struct yolk_file_browser
{
	// Yolk
	struct yolk *yk;

	// Current Path
	char path[VFS_PATH_MAXLEN];
	uint8_t path_len;

	// Selected Path
	char selected_path[VFS_PATH_MAXLEN];
	uint8_t selected_path_len;

	// Selected Name
	char *selected_name;
	uint8_t selected_name_len;

	// Selected Object Type
	uint8_t selected_otype;

	// Filter Extension
	char filter_ext[YOLK_FILE_BROWSER_FILTER_EXT_MAXLEN];
	uint8_t filter_ext_len;

	// Icon
	uint16_t icon;

	// Title
	char title[YOLK_FILE_BROWSER_TITLE_MAXLEN];
	uint8_t title_len;

	// Event Callback
	yolk_file_browser_event_cb_t event_cb;

	// User Data
	void *user;
};

// Enter File Browser
extern uint8_t yolk_file_browser_enter(struct yolk_file_browser *b, struct yolk *yk, char *root, char *ext, yolk_file_browser_event_cb_t event_cb, void *user, uint16_t icon, char *title_fmt, ...);

// Enter File Browser - Fixed Length
extern uint8_t yolk_file_browser_enter_n(struct yolk_file_browser *b, struct yolk *yk, char *root, uint8_t root_len, char *ext, uint8_t ext_len, yolk_file_browser_event_cb_t event_cb, void *user, uint16_t icon, char *title_fmt, ...);

// Enter File Browser - Fixed Length (variable argument list)
extern uint8_t yolk_file_browser_enter_n_v(struct yolk_file_browser *b, struct yolk *yk, char *root, uint8_t root_len, char *ext, uint8_t ext_len, yolk_file_browser_event_cb_t event_cb, void *user, uint16_t icon, char *title_fmt, va_list ap);

// Browse Selected Item
extern uint8_t yolk_file_browser_browse_selected(struct yolk_file_browser *b);

// Browse Back
extern uint8_t yolk_file_browser_browse_back(struct yolk_file_browser *b);

// Browse Current Path
extern uint8_t yolk_file_browser_browse_path(struct yolk_file_browser *b);

// Browse Mountpoints
extern uint8_t yolk_file_browser_browse_mountpoints(struct yolk_file_browser *b);

// Compute Selected Path
extern uint8_t yolk_file_browser_compute_selected_path(struct yolk_file_browser *b);

// Compute Path from Index
extern uint8_t yolk_file_browser_compute_path(struct yolk_file_browser *b, char *path, uint8_t index, char *buf, uint8_t *len, uint8_t *otype);

// Compute Path from Index - Fixed Length
extern uint8_t yolk_file_browser_compute_path_n(struct yolk_file_browser *b, char *path, uint8_t path_len, uint8_t index, char *buf, uint8_t *len, uint8_t *otype);

// Get Next Browse Item
extern uint8_t yolk_file_browser_next_browse_item(struct yolk_file_browser *b);

// Mountpoints Menu Event
extern void yolk_file_browser_mountpoints_menu_event(struct yolk *yk, struct yolk_file_browser *b, uint8_t event);

// Browse Menu Event
extern void yolk_file_browser_menu_event(struct yolk *yk, struct yolk_file_browser *b, uint8_t event);

#endif
