/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_AUTO_OFF_H
#define	__YOLK_AUTO_OFF_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "../yolk.h"

// Default Auto-Off Time
#define	YOLK_AUTO_OFF_DEFAULT_TIME					50000

// Auto-Off Structure
struct yolk_auto_off
{
	// Yolk
	struct yolk *yk;

	// Yolk Plugin
	struct yolk_plugin plugin;

	// Auto-Off Time
	uint32_t aoff_time;

	// Timer
	uint32_t timer;
};

// Attach Auto-Off (with default auto-off time)
extern void yolk_auto_off_attach(struct yolk_auto_off *aoff, struct yolk *yk);

// Attach Auto-Off with Time
extern void yolk_auto_off_attach_t(struct yolk_auto_off *aoff, struct yolk *yk, uint32_t time);

// Detach Auto-Off
extern void yolk_auto_off_detach(struct yolk_auto_off *aoff);

// Auto-Off Plugin Interface - Update
extern void yolk_auto_off_plugin_update(struct yolk *yk, struct yolk_auto_off *aoff);

#endif
