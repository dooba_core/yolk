/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_UI_H
#define	__YOLK_UI_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <gfx/gfx.h>
#include <gfx/dsp.h>
#include <gfx/col.h>
#include <gfx/img.h>
#include <gfx/txt.h>
#include <gfx/font.h>
#include <gfx/tileset.h>

// Internal Includes
#include "style.h"
#include "yolk.h"

// Style Shortcuts
#define	yolk_ui_margin(yk)								((yk)->style.ui_margin)
#define	yolk_ttl_col(yk)								((yk)->style.ttl_col)
#define	yolk_ttl_col_shadow(yk)							((yk)->style.ttl_col_shadow)
#define	yolk_txt_col(yk)								((yk)->style.txt_col)
#define	yolk_sel_col(yk)								((yk)->style.sel_col)
#define	yolk_csr_col(yk)								((yk)->style.csr_col)
#define	yolk_csr_col_shadow(yk)							((yk)->style.csr_col_shadow)
#define	yolk_wp_col(yk)									((yk)->style.wp_col)
#define	yolk_wp_mode(yk)								((yk)->style.wp_mode)
#define	yolk_wp_img_f(yk)								((yk)->style.wp_img_f)
#define	yolk_wp_img_fp(yk)								(&((yk)->style.wp_img_fp))
#define	yolk_wp_img_p(yk)								((yk)->style.wp_img_p)
#define	yolk_ttl_font(yk)								(&((yk)->style.ttl_font))
#define	yolk_txt_font(yk)								(&((yk)->style.txt_font))
#define	yolk_frame_tlst(yk)								(&((yk)->style.frame_tlst))
#define	yolk_spinner_tlst(yk)							(&((yk)->style.spinner_tlst))
#define	yolk_pbar_tlst(yk)								(&((yk)->style.pbar_tlst))
#define	yolk_icon_tlst(yk)								(&((yk)->style.icon_tlst))

// UI Shortcuts - Layout
#define	yolk_ui_head_h(yk)								(((yolk_ttl_font(yk)->fh > yolk_icon_tlst(yk)->th) ? yolk_ttl_font(yk)->fh : yolk_icon_tlst(yk)->th) + (yolk_ui_margin(yk) * 2))
#define	yolk_ui_head_baseline(yk)						((yk)->ui_yoff + (yolk_ui_head_h(yk) / 2))
#define	yolk_ui_head_icon_x(yk)							yolk_ui_margin(yk)
#define	yolk_ui_head_icon_y(yk)							(yolk_ui_head_baseline(yk) - (yolk_icon_tlst(yk)->th / 2))
#define	yolk_ui_head_title_x(yk)						(yolk_icon_tlst(yk)->tw + (yolk_ui_margin(yk) * 2))
#define	yolk_ui_head_title_y(yk)						(yolk_ui_head_baseline(yk) - (yolk_ttl_font(yk)->fh / 2))
#define	yolk_ui_head_title_w(yk)						(gfx_dsp_get_width((yk)->dsp) - (yolk_ui_head_title_x(yk) + yolk_ui_margin(yk)))
#define	yolk_ui_body_x(yk)								(yolk_ui_margin(yk) * 2)
#define	yolk_ui_body_y(yk)								((yk)->ui_yoff + yolk_ui_head_h(yk))
#define	yolk_ui_body_w(yk)								(gfx_dsp_get_width((yk)->dsp) - (yolk_ui_margin(yk) * 4))
#define	yolk_ui_body_h(yk)								((yk)->ui_ysize - yolk_ui_head_h(yk))
#define	yolk_ui_body_txt_lines(yk)						(yolk_ui_body_h(yk) / yolk_txt_font(yk)->fh)

// Invalidation Shortcuts
#define	yolk_ui_invalidate_all(yk)						gfx_dsp_invalidate((yk)->dsp, 0, 0, gfx_dsp_get_width((yk)->dsp), gfx_dsp_get_height((yk)->dsp))
#define	yolk_ui_invalidate_component(yk)				gfx_dsp_invalidate((yk)->dsp, 0, (yk)->ui_yoff, gfx_dsp_get_width(yk->dsp), (yk)->ui_ysize)
#define	yolk_ui_invalidate_component_head(yk)			gfx_dsp_invalidate((yk)->dsp, yolk_ui_body_x(yk), (yk)->ui_yoff, yolk_ui_body_w(yk), yolk_ui_head_h(yk))
#define	yolk_ui_invalidate_component_body(yk)			gfx_dsp_invalidate((yk)->dsp, yolk_ui_body_x(yk), yolk_ui_body_y(yk), yolk_ui_body_w(yk), yolk_ui_body_h(yk))

// UI On
extern void yolk_ui_on(struct yolk *yk);

// UI Off
extern void yolk_ui_off(struct yolk *yk);

// Re-Enter UI
extern void yolk_ui_reenter(struct yolk *yk);

// Rotate UI Left
extern void yolk_ui_rotate_left(struct yolk *yk);

// Rotate UI Right
extern void yolk_ui_rotate_right(struct yolk *yk);

// Set Component Area
extern void yolk_ui_set_component_area(struct yolk *yk, uint16_t yoff, uint16_t ysize);

// Set Active Component
extern void yolk_ui_set_component(struct yolk *yk, yolk_component_cb_t update, yolk_component_cb_t draw, yolk_component_cb_t reenter);

// Draw Generic Component Icon & Title
extern void yolk_ui_draw_component_head(struct yolk *yk, uint16_t icon, char *title, uint16_t title_len);

// Update UI
extern void yolk_ui_update(struct yolk *yk);

// Draw UI
extern void yolk_ui_draw(struct yolk *yk);

// Flush Draw UI (flushed invalidation stack)
extern void yolk_ui_flush_draw(struct yolk *yk);

#endif
