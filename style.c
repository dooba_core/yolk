/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>

// Internal Includes
#include "style.h"

// Initialize Style
void yolk_style_init(struct yolk_style *s)
{
	// Clear Everything
	memset(s, 0, sizeof(struct yolk_style));
}

// Set Defaults
void yolk_style_set_defaults(struct yolk_style *s)
{
	// Load Defaults
	yolk_style_set_ui_margin(s, YOLK_STYLE_DEFAULT_UI_MARGIN);
	yolk_style_set_ttl_col(s, YOLK_STYLE_DEFAULT_TTL_COL);
	yolk_style_set_ttl_shadow_col(s, YOLK_STYLE_DEFAULT_TTL_COL_SHADOW);
	yolk_style_set_txt_col(s, YOLK_STYLE_DEFAULT_TXT_COL);
	yolk_style_set_sel_col(s, YOLK_STYLE_DEFAULT_SEL_COL);
	yolk_style_set_csr_col(s, YOLK_STYLE_DEFAULT_CSR_COL);
	yolk_style_set_csr_shadow_col(s, YOLK_STYLE_DEFAULT_CSR_COL_SHADOW);
	yolk_style_set_wp_col(s, YOLK_STYLE_DEFAULT_WP_COL);
	yolk_style_set_wp_mode(s, YOLK_STYLE_WP_MODE_SOLID);
	yolk_style_set_ttl_font(s, YOLK_STYLE_DEFAULT_TTL_FONT);
	yolk_style_set_txt_font(s, YOLK_STYLE_DEFAULT_TXT_FONT);
	yolk_style_set_frame_tlst(s, YOLK_STYLE_DEFAULT_FRAME_TLST);
	yolk_style_set_spinner_tlst(s, YOLK_STYLE_DEFAULT_SPINNER_TLST);
	yolk_style_set_pbar_tlst(s, YOLK_STYLE_DEFAULT_PBAR_TLST);
	yolk_style_set_icon_tlst(s, YOLK_STYLE_DEFAULT_ICON_TLST);
}

// Set General UI Margin
void yolk_style_set_ui_margin(struct yolk_style *s, uint8_t margin)
{
	// Set Margin
	s->ui_margin = margin;
}

// Set Title Color
void yolk_style_set_ttl_col(struct yolk_style *s, uint16_t col)
{
	// Set Color
	s->ttl_col = col;
}

// Set Title Shadow Color
void yolk_style_set_ttl_shadow_col(struct yolk_style *s, uint16_t col)
{
	// Set Color
	s->ttl_col_shadow = col;
}

// Set Text Color
void yolk_style_set_txt_col(struct yolk_style *s, uint16_t col)
{
	// Set Color
	s->txt_col = col;
}

// Set Selection Color
void yolk_style_set_sel_col(struct yolk_style *s, uint16_t col)
{
	// Set Color
	s->sel_col = col;
}

// Set Text Cursor Color
void yolk_style_set_csr_col(struct yolk_style *s, uint16_t col)
{
	// Set Color
	s->csr_col = col;
}

// Set Text Cursor Shadow Color
void yolk_style_set_csr_shadow_col(struct yolk_style *s, uint16_t col)
{
	// Set Color
	s->csr_col_shadow = col;
}

// Set Wallpaper Color
void yolk_style_set_wp_col(struct yolk_style *s, uint16_t col)
{
	// Set Color
	s->wp_col = col;
}

// Set Wallpaper Mode
void yolk_style_set_wp_mode(struct yolk_style *s, uint8_t mode)
{
	// Set Mode
	s->wp_mode = mode;
}

// Set Wallpaper Image - File
void yolk_style_set_wp_img_f(struct yolk_style *s, char *path)
{
	// Set Wallpaper
	yolk_style_set_wp_img_f_n(s, path, strlen(path));
}

// Set Wallpaper Image - File (Fixed Length)
void yolk_style_set_wp_img_f_n(struct yolk_style *s, char *path, uint8_t path_len)
{
	// Clear Previous Wallpaper Image
	if(s->wp_img_f_len)																				{ vfs_close(&(s->wp_img_fp)); s->wp_img_f_len = 0; }

	// Attempt to Open File
	if(vfs_open_n(&(s->wp_img_fp), path, path_len, 0))												{ return; }

	// Set Wallpaper Image
	s->wp_img_f_len = path_len;
}

// Set Wallpaper Image - Flash
void yolk_style_set_wp_img_p(struct yolk_style *s, uint32_t data)
{
	// Set Image
	s->wp_img_p = data;
}

// Set Title Font
void yolk_style_set_ttl_font(struct yolk_style *s, uint32_t data)
{
	// Load Font
	gfx_font_init(&(s->ttl_font), data);
}

// Set Text Font
void yolk_style_set_txt_font(struct yolk_style *s, uint32_t data)
{
	// Load Font
	gfx_font_init(&(s->txt_font), data);
}

// Set Frame Tileset
void yolk_style_set_frame_tlst(struct yolk_style *s, uint32_t data)
{
	// Load Tileset
	gfx_tileset_init(&(s->frame_tlst), data);
}

// Set Spinner Tileset
void yolk_style_set_spinner_tlst(struct yolk_style *s, uint32_t data)
{
	// Load Tileset
	gfx_tileset_init(&(s->spinner_tlst), data);
}

// Set Progress Bar Tileset
void yolk_style_set_pbar_tlst(struct yolk_style *s, uint32_t data)
{
	// Load Tileset
	gfx_tileset_init(&(s->pbar_tlst), data);
}

// Set Icon Tileset
void yolk_style_set_icon_tlst(struct yolk_style *s, uint32_t data)
{
	// Load Tileset
	gfx_tileset_init(&(s->icon_tlst), data);
}
