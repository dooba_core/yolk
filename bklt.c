/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <dio/dio.h>
#include <pwm/pwm.h>

// Internal Includes
#include "bklt.h"

// Initialize Backlight
void yolk_bklt_init(struct yolk_bklt *bklt, uint8_t mode, uint8_t pin, uint8_t base, uint8_t speed)
{
	// Init Backlight
	bklt->mode = mode;
	bklt->pin = pin;
	bklt->base = base;
	bklt->speed = speed;

	// Turn on
	bklt->trgt = base;
	bklt->cval = 0;

	// Apply Value
	yolk_bklt_apply(bklt);
}

// Update Backlight
void yolk_bklt_update(struct yolk_bklt *bklt)
{
	// Update Backlight
	if(bklt->cval < bklt->trgt)								{ bklt->cval = ((bklt->trgt - bklt->cval) > bklt->speed) ? (bklt->cval + bklt->speed) : bklt->trgt; }
	else if(bklt->cval > bklt->trgt)						{ bklt->cval = ((bklt->cval - bklt->trgt) > bklt->speed) ? (bklt->cval - bklt->speed) : bklt->trgt; }
	else													{ return; }

	// Apply
	yolk_bklt_apply(bklt);
}

// Target Base
void yolk_bklt_target_base(struct yolk_bklt *bklt)
{
	// Set Target
	yolk_bklt_set_target(bklt, bklt->base);
}

// Target Zero
void yolk_bklt_target_zero(struct yolk_bklt *bklt)
{
	// Set Target
	yolk_bklt_set_target(bklt, 0);
}

// Set Backlight Target
void yolk_bklt_set_target(struct yolk_bklt *bklt, uint8_t target)
{
	// Set Target
	bklt->trgt = target;
}

// Apply Backlight
void yolk_bklt_apply(struct yolk_bklt *bklt)
{
	// Check Mode
	if(bklt->mode == YOLK_BKLT_MODE_PWM)
	{
		// Set PWM
		if(bklt->cval)										{ pwm_set(bklt->pin, bklt->cval); }
		else												{ pwm_off(bklt->pin); }
	}
	else if(bklt->mode == YOLK_BKLT_MODE_DIO)
	{
		// Set DIO
		if(bklt->cval == 0)									{ dio_lo(bklt->pin); }
		else												{ dio_hi(bklt->pin); }
	}
	else													{ /* NoOp */ }
}
