/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <util/cprintf.h>
#include <gfx/txt.h>
#include <gfx/tileset.h>

// Internal Includes
#include "menu.h"

// Enter Menu
void yolk_menu_enter(struct yolk *yk, yolk_menu_event_cb_t event_cb, void *user, uint16_t icon, char *title_fmt, ...)
{
	va_list ap;
	struct yolk_menu_data *d;
	uint16_t size;

	// Acquire Args
	va_start(ap, title_fmt);

	// Acquire Menu Data
	d = (struct yolk_menu_data *)(yk->component_data);
	size = yk->component_data_size;

	// Setup Menu
	d->event_cb = event_cb;
	d->user = user;
	d->icon = icon;

	// Set Title
	d->title = (char *)&(yk->component_data[sizeof(struct yolk_menu_data)]);
	size = size - sizeof(struct yolk_menu_data);
	d->title_len = cvsnprintf(d->title, size, title_fmt, ap);

	// Set Items (reserve at least half of data for item names)
	d->items = (struct yolk_menu_item *)&(yk->component_data[sizeof(struct yolk_menu_data) + d->title_len]);
	size = size - d->title_len;
	d->items_size = (((size / 2) / sizeof(struct yolk_menu_item)) > YOLK_MENU_MAX_ITEMS) ? YOLK_MENU_MAX_ITEMS : ((size / 2) / sizeof(struct yolk_menu_item));
	d->item_count = 0;

	// Set Next Name Pointer
	d->next_name = (char *)&(yk->component_data[sizeof(struct yolk_menu_data) + d->title_len + (d->items_size * sizeof(struct yolk_menu_item))]);
	size = size - (d->items_size * sizeof(struct yolk_menu_item));
	d->size_left = size;

	// "Re-Enter" Component
	yolk_menu_reenter(yk, d, sizeof(struct yolk_menu_data));

	// Invalidate UI Component Area
	yolk_ui_invalidate_component(yk);

	// Set Active Component
	yolk_ui_set_component(yk, (yolk_component_cb_t)yolk_menu_update, (yolk_component_cb_t)yolk_menu_draw, (yolk_component_cb_t)yolk_menu_reenter);

	// Release Args
	va_end(ap);
}

// Add Item
void yolk_menu_add_item(struct yolk *yk, uint32_t value, uint16_t icon, char *fmt, ...)
{
	va_list ap;
	struct yolk_menu_data *d;
	struct yolk_menu_item *item;
	uint16_t name_size;

	// Acquire Args
	va_start(ap, fmt);

	// Acquire Menu Data
	d = (struct yolk_menu_data *)(yk->component_data);

	// Check for available item space
	if(d->item_count < d->items_size)
	{
		// Acquire Item Pointer
		item = &(d->items[d->item_count]);
		d->item_count = d->item_count + 1;

		// Set Name
		item->name = d->next_name;
		name_size = d->size_left;
		if(name_size > YOLK_MENU_ITEM_NAME_MAXLEN)											{ name_size = YOLK_MENU_ITEM_NAME_MAXLEN; }
		item->name_len = cvsnprintf(item->name, name_size, fmt, ap);
		d->next_name = &(d->next_name[item->name_len]);
		d->size_left = d->size_left - item->name_len;

		// Configure Item
		item->value = value;
		item->icon = icon;
	}

	// Release Args
	va_end(ap);
}

// Update Menu
void yolk_menu_update(struct yolk *yk, struct yolk_menu_data *data, uint16_t data_size)
{
	uint8_t event;

	// Process Menu Input
	event = yolk_menu_process(yk, data);

	// Pass Event
	if((event != YOLK_MENU_EVENT_NONE) && (data->event_cb))									{ data->event_cb(yk, data->user, event); }
}

// Process Menu
uint8_t yolk_menu_process(struct yolk *yk, struct yolk_menu_data *data)
{
	// Process Input
	if(yolk_in_up(yk))																		{ yolk_menu_up(yk, data); return YOLK_MENU_EVENT_UP; }
	else if(yolk_in_down(yk))																{ yolk_menu_down(yk, data); return YOLK_MENU_EVENT_DOWN; }
	else if(yolk_in_left(yk))																{ return YOLK_MENU_EVENT_BACK; }
	else if(yolk_in_right(yk))																{ return YOLK_MENU_EVENT_RIGHT; }
	else if(yolk_in_center(yk))																{ return YOLK_MENU_EVENT_SELECT; }
	else if(yolk_in_ok(yk))																	{ return YOLK_MENU_EVENT_OPTION; }
	else if(yolk_in_cancel(yk))																{ return YOLK_MENU_EVENT_CANCEL; }
	else																					{ /* NoOp */ }

	return YOLK_MENU_EVENT_NONE;
}

// Invalidate Items
void yolk_menu_invalidate_items(struct yolk *yk, struct yolk_menu_data *data)
{
	uint8_t i;

	// Run through Items
	i = 0;
	while((i < data->dsp_count) && ((data->dsp_start + i) < data->item_count))
	{
		// Invalidate
		gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
		gfx_dsp_invalidate(yk->dsp, yolk_menu_item_name_x(yk), yolk_menu_item_name_y(yk, i), yolk_menu_item_name_w(yk), yolk_txt_font(yk)->fh);

		// Loop
		i = i + 1;
	}
}

// Go up in Menu
void yolk_menu_up(struct yolk *yk, struct yolk_menu_data *data)
{
	uint16_t i;

	// Up
	if(data->selected == 0)																	{ return; }
	data->selected = data->selected - 1;
	if(data->selected < data->dsp_start)													{ data->dsp_start = data->selected; yolk_menu_invalidate_items(yk, data); return; }

	// Invalidate
	i = data->selected - data->dsp_start;
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_name_x(yk), yolk_menu_item_name_y(yk, i), yolk_menu_item_name_w(yk), yolk_txt_font(yk)->fh);
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x_sel(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
	i = i + 1;
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_name_x(yk), yolk_menu_item_name_y(yk, i), yolk_menu_item_name_w(yk), yolk_txt_font(yk)->fh);
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x_sel(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
}

// Go down in Menu
void yolk_menu_down(struct yolk *yk, struct yolk_menu_data *data)
{
	uint16_t i;

	// Down
	if((data->item_count < 2) || (data->selected == (data->item_count - 1)))				{ return; }
	data->selected = data->selected + 1;
	if(data->selected >= (data->dsp_start + data->dsp_count))								{ data->dsp_start = (data->selected - data->dsp_count) + 1; yolk_menu_invalidate_items(yk, data); return; }

	// Invalidate
	i = (data->selected - 1) - data->dsp_start;
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_name_x(yk), yolk_menu_item_name_y(yk, i), yolk_menu_item_name_w(yk), yolk_txt_font(yk)->fh);
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x_sel(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
	i = i + 1;
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_name_x(yk), yolk_menu_item_name_y(yk, i), yolk_menu_item_name_w(yk), yolk_txt_font(yk)->fh);
	gfx_dsp_invalidate(yk->dsp, yolk_menu_item_icon_x_sel(yk), yolk_menu_item_icon_y(yk, i), yolk_icon_tlst(yk)->tw, yolk_icon_tlst(yk)->th);
}

// Draw Menu
void yolk_menu_draw(struct yolk *yk, struct yolk_menu_data *data, uint16_t data_size)
{
	uint8_t i;
	struct yolk_menu_item *item;

	// Draw Head
	yolk_ui_draw_component_head(yk, data->icon, data->title, data->title_len);

	// Run through Items
	i = 0;
	while((i < data->dsp_count) && ((data->dsp_start + i) < data->item_count))
	{
		// Acquire Item
		item = &(data->items[data->dsp_start + i]);

		// Draw Icon
		gfx_tileset_draw(yk->dsp, yolk_icon_tlst(yk), item->icon, yolk_menu_item_icon_x(yk), yolk_menu_item_icon_y(yk, i));

		// Draw Name
		gfx_txt_n(yk->dsp, item->name, item->name_len, yolk_menu_item_name_x(yk), yolk_menu_item_name_y(yk, i), yolk_menu_item_name_w(yk), 0, yolk_txt_font(yk), ((data->selected == (data->dsp_start + i)) ? yolk_sel_col(yk) : yolk_txt_col(yk)));

		// Check Selected { Draw Selection Marker }
		if(data->selected == (data->dsp_start + i))											{ gfx_tileset_draw(yk->dsp, yolk_icon_tlst(yk), YOLK_ICON_ARROW_L, yolk_menu_item_icon_x_sel(yk), yolk_menu_item_icon_y(yk, i)); }

		// Loop
		i = i + 1;
	}
}

// Re-Enter Menu
void yolk_menu_reenter(struct yolk *yk, struct yolk_menu_data *data, uint16_t data_size)
{
	// Reset Selected & Display Range
	data->selected = 0;
	data->dsp_start = 0;
	data->dsp_count = yolk_ui_body_h(yk) / yolk_menu_item_h(yk);
	if(data->dsp_count > YOLK_MENU_MAX_DSP_COUNT)											{ data->dsp_count = YOLK_MENU_MAX_DSP_COUNT; }
}
