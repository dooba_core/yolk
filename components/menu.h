/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_MENU_H
#define	__YOLK_MENU_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "../ui.h"
#include "../yolk.h"

// Menu Events
#define	YOLK_MENU_EVENT_NONE							0
#define	YOLK_MENU_EVENT_SELECT							1
#define	YOLK_MENU_EVENT_OPTION							2
#define	YOLK_MENU_EVENT_RIGHT							3
#define	YOLK_MENU_EVENT_BACK							4
#define	YOLK_MENU_EVENT_CANCEL							5
#define	YOLK_MENU_EVENT_UP								6
#define	YOLK_MENU_EVENT_DOWN							7

// Max Items
#ifndef	YOLK_MENU_MAX_ITEMS
#define	YOLK_MENU_MAX_ITEMS								150
#endif

// Item Name Max Length
#ifndef	YOLK_MENU_ITEM_NAME_MAXLEN
#define	YOLK_MENU_ITEM_NAME_MAXLEN						12
#endif

// Maximum Item Display Count
#ifndef	YOLK_MENU_MAX_DSP_COUNT
#define	YOLK_MENU_MAX_DSP_COUNT							4
#endif

// Layout Shortcuts
#define	yolk_menu_item_h(yk)							(((yolk_txt_font(yk)->fh > yolk_icon_tlst(yk)->th) ? yolk_txt_font(yk)->fh : yolk_icon_tlst(yk)->th) + (yolk_ui_margin(yk) * 2))
#define	yolk_menu_item_x(yk)							yolk_ui_body_x(yk)
#define	yolk_menu_item_y(yk, i)							(yolk_ui_body_y(yk) + (yolk_menu_item_h(yk) * (i)))
#define	yolk_menu_item_w(yk)							yolk_ui_body_w(yk)
#define	yolk_menu_item_baseline(yk, i)					(yolk_menu_item_y(yk, i) + (yolk_menu_item_h(yk) / 2))
#define	yolk_menu_item_icon_x(yk)						yolk_ui_body_x(yk)
#define	yolk_menu_item_icon_x_sel(yk)					((yolk_ui_body_x(yk) + yolk_ui_body_w(yk)) - yolk_icon_tlst(yk)->tw)
#define	yolk_menu_item_icon_y(yk, i)					(yolk_menu_item_baseline(yk, i) - (yolk_icon_tlst(yk)->th / 2))
#define	yolk_menu_item_name_x(yk)						(yolk_menu_item_icon_x(yk) + yolk_icon_tlst(yk)->tw + yolk_ui_margin(yk))
#define	yolk_menu_item_name_y(yk, i)					(yolk_menu_item_baseline(yk, i) - (yolk_txt_font(yk)->fh / 2))
#define	yolk_menu_item_name_w(yk)						(yolk_menu_item_icon_x_sel(yk) - (yolk_menu_item_name_x(yk) + yolk_ui_margin(yk)))

// Item Access Shortcuts
#define	yolk_menu_selected(yk)							(((struct yolk_menu_data *)((yk)->component_data))->selected)
#define	yolk_menu_selected_item(yk)						((struct yolk_menu_item *)&(((struct yolk_menu_data *)((yk)->component_data))->items[yolk_menu_selected(yk)]))
#define	yolk_menu_selected_item_value(yk)				(yolk_menu_selected_item(yk)->value)

// Event Callback Type
typedef	void (*yolk_menu_event_cb_t)(struct yolk *yk, void *user, uint8_t event);

// Menu Item Structure
struct yolk_menu_item
{
	// Name
	char *name;
	uint8_t name_len;

	// Item Value
	uint32_t value;

	// Icon Tile
	uint16_t icon;
};

// Menu Data Structure
struct yolk_menu_data
{
	// Event Callback
	yolk_menu_event_cb_t event_cb;

	// User Data
	void *user;

	// Icon
	uint16_t icon;

	// Title
	char *title;
	uint16_t title_len;

	// Items
	struct yolk_menu_item *items;
	uint8_t items_size;
	uint8_t item_count;

	// Next Name Pointer
	char *next_name;
	uint16_t size_left;

	// Selected Item
	uint8_t selected;

	// Item Display Range
	uint8_t dsp_start;
	uint8_t dsp_count;
	uint8_t max_dsp_count;
};

// Enter Menu
extern void yolk_menu_enter(struct yolk *yk, yolk_menu_event_cb_t event_cb, void *user, uint16_t icon, char *title_fmt, ...);

// Add Item
extern void yolk_menu_add_item(struct yolk *yk, uint32_t value, uint16_t icon, char *fmt, ...);

// Update Menu
extern void yolk_menu_update(struct yolk *yk, struct yolk_menu_data *data, uint16_t data_size);

// Process Menu
extern uint8_t yolk_menu_process(struct yolk *yk, struct yolk_menu_data *data);

// Invalidate Items
extern void yolk_menu_invalidate_items(struct yolk *yk, struct yolk_menu_data *data);

// Go up in Menu
extern void yolk_menu_up(struct yolk *yk, struct yolk_menu_data *data);

// Go down in Menu
extern void yolk_menu_down(struct yolk *yk, struct yolk_menu_data *data);

// Draw Menu
extern void yolk_menu_draw(struct yolk *yk, struct yolk_menu_data *data, uint16_t data_size);

// Re-Enter Menu
extern void yolk_menu_reenter(struct yolk *yk, struct yolk_menu_data *data, uint16_t data_size);

#endif
