/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_DIALOG_H
#define	__YOLK_DIALOG_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "../ui.h"
#include "../yolk.h"

// Standard Buttons
#define	YOLK_DIALOG_STD_BTN_NONE						0x00
#define	YOLK_DIALOG_STD_BTN_YES_NO						0x01
#define	YOLK_DIALOG_STD_BTN_OK_CANCEL					0x02
#define	YOLK_DIALOG_STD_BTN_OK							0x03

// Layout Shortcuts
#define	yolk_dialog_txt_h(yk)							yolk_ui_body_h(yk)
#define	yolk_dialog_btn_y(yk)							((yolk_ui_body_y(yk) + yolk_ui_body_h(yk)) - (yolk_txt_font(yk)->fh + yolk_ui_margin(yk)))
#define	yolk_dialog_btn_x_1(yk)							yolk_ui_body_x(yk)
#define	yolk_dialog_btn_x_2(yk, btn)					((yolk_ui_body_x(yk) + yolk_ui_body_w(yk)) - (yolk_txt_font(yk)->fw * strlen(btn)))

// Update Callback Type
typedef	void (*yolk_dialog_cb_t)(struct yolk *yk, void *user);

// Dialog Data Structure
struct yolk_dialog_data
{
	// Update Callback
	yolk_dialog_cb_t update_cb;

	// Standard Buttons
	uint8_t std_btn;

	// Standard Actions
	yolk_dialog_cb_t action_1;
	yolk_dialog_cb_t action_2;

	// User Data
	void *user;

	// Icon
	uint16_t icon;

	// Title
	char *title;
	uint16_t title_len;

	// Text
	char *text;
	uint16_t text_len;
	uint16_t text_max_len;
};

// Enter Dialog
extern void yolk_dialog_enter(struct yolk *yk, yolk_dialog_cb_t update_cb, void *user, uint16_t icon, char *title_fmt, char *text_fmt, ...);

// Enter Dialog with standard action buttons
extern void yolk_dialog_enter_std_btn(struct yolk *yk, yolk_dialog_cb_t update_cb, uint8_t btn, yolk_dialog_cb_t action_1, yolk_dialog_cb_t action_2, void *user, uint16_t icon, char *title_fmt, char *text_fmt, ...);

// Enter Dialog with standard action buttons (va_list)
extern void yolk_dialog_enter_std_btn_v(struct yolk *yk, yolk_dialog_cb_t update_cb, uint8_t btn, yolk_dialog_cb_t action_1, yolk_dialog_cb_t action_2, void *user, uint16_t icon, char *title_fmt, char *text_fmt, va_list ap);

// Update Icon
extern void yolk_dialog_set_icon(struct yolk *yk, uint16_t icon);

// Update Text
extern void yolk_dialog_set_text(struct yolk *yk, char *fmt, ...);

// Update Dialog
extern void yolk_dialog_update(struct yolk *yk, struct yolk_dialog_data *data, uint16_t data_size);

// Draw Dialog
extern void yolk_dialog_draw(struct yolk *yk, struct yolk_dialog_data *data, uint16_t data_size);

#endif
