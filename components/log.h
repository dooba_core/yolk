/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_LOG_H
#define	__YOLK_LOG_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <gfx/txt.h>

// Internal Includes
#include "../ui.h"
#include "../yolk.h"

// Layout Shortcuts
#define	yolk_log_txt_h(yk)							yolk_ui_body_h(yk)

// Minimum guaranteed size for a message
#define	YOLK_LOG_MSG_MIN_LEN						64

// Max length for a message
#define	YOLK_LOG_MSG_MAX_LEN						128

// Max total length
#define	YOLK_LOG_MAX_LEN							GFX_TXT_BUFSIZE

// Maximum message count
#define	YOLK_LOG_MAX_MSG_COUNT						20

// Update Callback Type
typedef	void (*yolk_log_cb_t)(struct yolk *yk, void *user);

// Log Data Structure
struct yolk_log_data
{
	// Update Callback
	yolk_log_cb_t update_cb;

	// Standard Actions
	yolk_log_cb_t action_1;
	yolk_log_cb_t action_2;

	// User Data
	void *user;

	// Icon
	uint16_t icon;

	// Title
	char *title;
	uint16_t title_len;

	// Text
	char *text;
	uint16_t text_len;
	uint16_t text_max_len;
	uint8_t msg_count;
};

// Enter Log
extern void yolk_log_enter(struct yolk *yk, yolk_log_cb_t update_cb, void *user, uint16_t icon, char *title_fmt, ...);

// Enter Log with standard actions
extern void yolk_log_enter_std_act(struct yolk *yk, yolk_log_cb_t update_cb, yolk_log_cb_t action_1, yolk_log_cb_t action_2, void *user, uint16_t icon, char *title_fmt, ...);

// Enter Log with standard actions (va_list)
extern void yolk_log_enter_std_act_v(struct yolk *yk, yolk_log_cb_t update_cb, yolk_log_cb_t action_1, yolk_log_cb_t action_2, void *user, uint16_t icon, char *title_fmt, va_list ap);

// Update Icon
extern void yolk_log_set_icon(struct yolk *yk, uint16_t icon);

// Print
extern void yolk_log_printf(struct yolk *yk, char *fmt, ...);

// Update Log
extern void yolk_log_update(struct yolk *yk, struct yolk_log_data *data, uint16_t data_size);

// Draw Log
extern void yolk_log_draw(struct yolk *yk, struct yolk_log_data *data, uint16_t data_size);

#endif
