/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <util/cprintf.h>
#include <gfx/txt.h>
#include <gfx/pbar.h>
#include <gfx/tileset.h>

// Internal Includes
#include "spinwait.h"

// Enter Spinwait
void yolk_spinwait_enter(struct yolk *yk, yolk_spinwait_update_cb_t update_cb, void *user, uint16_t icon, uint8_t show_progress, char *title_fmt, char *text_fmt, ...)
{
	va_list ap;
	struct yolk_spinwait_data *d;
	uint16_t size;

	// Acquire Args
	va_start(ap, text_fmt);

	// Acquire Spinwait Data
	d = (struct yolk_spinwait_data *)(yk->component_data);
	size = yk->component_data_size;

	// Setup Spinwait
	d->update_cb = update_cb;
	d->user = user;
	d->icon = icon;
	d->show_progress = show_progress;
	d->progress = 0;
	d->frame = 0;
	d->counter = 0;

	// Set Title
	d->title = (char *)&(yk->component_data[sizeof(struct yolk_spinwait_data)]);
	size = size - sizeof(struct yolk_spinwait_data);
	d->title_len = cvpsnprintf(d->title, size, title_fmt, &ap);

	// Set Text
	d->text = (char *)&(yk->component_data[sizeof(struct yolk_spinwait_data) + d->title_len]);
	d->text_size = size - d->title_len;
	d->text_len = cvpsnprintf(d->text, d->text_size, text_fmt, &ap);

	// Invalidate UI Component Area
	yolk_ui_invalidate_component(yk);

	// Set Active Component
	yolk_ui_set_component(yk, (yolk_component_cb_t)yolk_spinwait_update, (yolk_component_cb_t)yolk_spinwait_draw, 0);

	// Release Args
	va_end(ap);
}

// Update Spinwait
void yolk_spinwait_update(struct yolk *yk, struct yolk_spinwait_data *data, uint16_t data_size)
{
	// Inc Counter
	data->counter = data->counter + 1;
	if(data->counter >= YOLK_SPINWAIT_SPEED)
	{
		// Invalidate Spinner & Inc Frame
		gfx_dsp_invalidate(yk->dsp, yolk_spinwait_spin_x(yk, data), yolk_spinwait_spin_y(yk, data), yolk_spinner_tlst(yk)->tw, yolk_spinner_tlst(yk)->th);
		data->frame = (data->frame + 1) % yolk_spinner_tlst(yk)->tiles;
		data->counter = 0;
	}

	// Call User Update
	if(data->update_cb)								{ data->update_cb(yk, data->user); }
}

// Set Text
void yolk_spinwait_set_text(struct yolk *yk, char *fmt, ...)
{
	va_list ap;
	struct yolk_spinwait_data *d;

	// Acquire Args
	va_start(ap, fmt);

	// Acquire Spinwait Data
	d = (struct yolk_spinwait_data *)(yk->component_data);

	// Set Text
	d->text_len = cvpsnprintf(d->text, d->text_size, fmt, &ap);

	// Invalidate Text Area
	gfx_dsp_invalidate(yk->dsp, yolk_spinwait_text_x(yk), yolk_spinwait_text_y(yk, d), yolk_spinwait_text_w(yk), yolk_spinwait_text_h(yk, d));

	// Release Args
	va_end(ap);
}

// Set Progress Visibility
void yolk_spinwait_set_progress_visible(struct yolk *yk, uint8_t visible)
{
	struct yolk_spinwait_data *d;

	// Acquire Spinwait Data
	d = (struct yolk_spinwait_data *)(yk->component_data);

	// Set Progress Visibility
	d->show_progress = visible;

	// Invalidate Progress Bar Area
	gfx_dsp_invalidate(yk->dsp, yolk_spinwait_pbar_x(yk), yolk_spinwait_pbar_y(yk, d), yolk_spinwait_pbar_w(yk), yolk_pbar_tlst(yk)->th);
}

// Set Progress
void yolk_spinwait_set_progress(struct yolk *yk, uint8_t progress)
{
	struct yolk_spinwait_data *d;

	// Acquire Spinwait Data
	d = (struct yolk_spinwait_data *)(yk->component_data);

	// Set Progress
	d->progress = progress;

	// Invalidate Progress Bar Area
	gfx_dsp_invalidate(yk->dsp, yolk_spinwait_pbar_x(yk), yolk_spinwait_pbar_y(yk, d), yolk_spinwait_pbar_w(yk), yolk_pbar_tlst(yk)->th);
}

// Draw Spinwait
void yolk_spinwait_draw(struct yolk *yk, struct yolk_spinwait_data *data, uint16_t data_size)
{
	// Draw Head
	yolk_ui_draw_component_head(yk, data->icon, data->title, data->title_len);

	// Draw Spinner
	gfx_tileset_draw(yk->dsp, yolk_spinner_tlst(yk), data->frame, yolk_spinwait_spin_x(yk, data), yolk_spinwait_spin_y(yk, data));

	// Draw Progress
	if(data->show_progress)							{ gfx_pbar_draw_default(yk->dsp, yolk_pbar_tlst(yk), data->progress, yolk_spinwait_pbar_w(yk), yolk_spinwait_pbar_x(yk), yolk_spinwait_pbar_y(yk, data)); }

	// Draw Text
	gfx_txt_n(yk->dsp, data->text, data->text_len, yolk_spinwait_text_x(yk), yolk_spinwait_text_y(yk, data), yolk_spinwait_text_w(yk), yolk_spinwait_text_h(yk, data), yolk_txt_font(yk), yolk_txt_col(yk));
}
