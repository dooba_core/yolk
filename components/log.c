/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>
#include <gfx/tileset.h>

// Internal Includes
#include "log.h"

// Enter Log
void yolk_log_enter(struct yolk *yk, yolk_log_cb_t update_cb, void *user, uint16_t icon, char *title_fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, title_fmt);

	// Enter
	yolk_log_enter_std_act_v(yk, update_cb, 0, 0, user, icon, title_fmt, ap);

	// Release Args
	va_end(ap);
}

// Enter Log with standard actions
void yolk_log_enter_std_act(struct yolk *yk, yolk_log_cb_t update_cb, yolk_log_cb_t action_1, yolk_log_cb_t action_2, void *user, uint16_t icon, char *title_fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, title_fmt);

	// Enter
	yolk_log_enter_std_act_v(yk, update_cb, action_1, action_2, user, icon, title_fmt, ap);

	// Release Args
	va_end(ap);
}

// Enter Log with standard actions (va_list)
void yolk_log_enter_std_act_v(struct yolk *yk, yolk_log_cb_t update_cb, yolk_log_cb_t action_1, yolk_log_cb_t action_2, void *user, uint16_t icon, char *title_fmt, va_list ap)
{
	struct yolk_log_data *d;
	uint16_t size;

	// Acquire Log Data
	d = (struct yolk_log_data *)(yk->component_data);
	size = yk->component_data_size - sizeof(struct yolk_log_data);

	// Setup Log
	d->action_1 = action_1;
	d->action_2 = action_2;
	d->update_cb = update_cb;
	d->user = user;
	d->icon = icon;
	d->msg_count = 0;

	// Set Title
	d->title = (char *)&(yk->component_data[sizeof(struct yolk_log_data)]);
	d->title_len = cvpsnprintf(d->title, size, title_fmt, &ap);
	size = size - d->title_len;

	// Set Text
	d->text = (char *)&(yk->component_data[sizeof(struct yolk_log_data) + d->title_len]);
	d->text_max_len = size;
	d->text_len = 0;

	// Invalidate UI Component Area
	yolk_ui_invalidate_component(yk);

	// Set Active Component
	yolk_ui_set_component(yk, (yolk_component_cb_t)yolk_log_update, (yolk_component_cb_t)yolk_log_draw, 0);
}

// Update Icon
void yolk_log_set_icon(struct yolk *yk, uint16_t icon)
{
	struct yolk_log_data *d;

	// Acquire Log Data & Set Icon
	d = (struct yolk_log_data *)(yk->component_data);
	d->icon = icon;

	// Invalidate UI Component Head Area
	yolk_ui_invalidate_component_head(yk);
}

// Print Text
void yolk_log_printf(struct yolk *yk, char *fmt, ...)
{
	va_list ap;
	struct yolk_log_data *d;
	uint16_t l;
	uint16_t i;

	// Acquire Args
	va_start(ap, fmt);

	// Acquire Log Data
	d = (struct yolk_log_data *)(yk->component_data);

	// Check available size
	while(((d->text_max_len - d->text_len) < YOLK_LOG_MSG_MIN_LEN) || (d->msg_count >= YOLK_LOG_MAX_MSG_COUNT) || ((d->text_len + YOLK_LOG_MSG_MAX_LEN) >= YOLK_LOG_MAX_LEN))
	{
		// Remove message
		l = str_chr(d->text, d->text_len, '\n');
		if(l < d->text_len)																									{ l = l + 1; }
		for(i = 0; i < (d->text_len - l); i = i + 1)																		{ d->text[i] = d->text[l + i]; }
		d->text_len = d->text_len - l;
		d->msg_count = d->msg_count - 1;
	}

	// Check text
	if(d->text_len)
	{
		// Add delim
		d->text[d->text_len] = '\n';
		d->text_len = d->text_len + 1;
	}

	// Print
	i = d->text_max_len - d->text_len;
	if(i > YOLK_LOG_MSG_MAX_LEN)																							{ i = YOLK_LOG_MSG_MAX_LEN; }
	l = cvpsnprintf(&(d->text[d->text_len]), i, fmt, &ap);
	d->text_len = d->text_len + l;
	d->msg_count = d->msg_count + 1;

	// Release Args
	va_end(ap);

	// Invalidate UI Component Area
	yolk_ui_invalidate_component_body(yk);
}

// Update Log
void yolk_log_update(struct yolk *yk, struct yolk_log_data *data, uint16_t data_size)
{
	// Pass Update to Callback
	if(data->update_cb)																										{ data->update_cb(yk, data->user); }
	else
	{
		// Handle Standard Actions
		if((yolk_in_center(yk) || yolk_in_u1(yk) || yolk_in_right(yk)) && (data->action_1))									{ data->action_1(yk, data->user); }
		if((yolk_in_u2(yk) || yolk_in_left(yk)) && (data->action_2))														{ data->action_2(yk, data->user); }
	}
}

// Draw Log
void yolk_log_draw(struct yolk *yk, struct yolk_log_data *data, uint16_t data_size)
{
	uint8_t skip;
	uint16_t px;
	uint16_t py;

	// Draw Head
	yolk_ui_draw_component_head(yk, data->icon, data->title, data->title_len);

	// Check Text
	if(data->text_len == 0)																									{ return; }

	// Draw Text
	skip = 0;
	while(gfx_txt_itoc(yk->dsp, yolk_ui_body_x(yk), yolk_ui_body_y(yk), yolk_ui_body_w(yk), yolk_log_txt_h(yk), skip, yolk_txt_font(yk), data->text, data->text_len, data->text_len - 1, &px, &py))		{ skip = skip + 1; }
	gfx_txt_s_n(yk->dsp, data->text, data->text_len, yolk_ui_body_x(yk), yolk_ui_body_y(yk), yolk_ui_body_w(yk), yolk_log_txt_h(yk), skip, yolk_txt_font(yk), yolk_txt_col(yk));
}
