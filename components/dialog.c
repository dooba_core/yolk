/* Dooba SDK
 * Generic GUI Framework
 */

// External Includes
#include <string.h>
#include <util/cprintf.h>
#include <gfx/txt.h>
#include <gfx/tileset.h>

// Internal Includes
#include "dialog.h"

// Enter Dialog
void yolk_dialog_enter(struct yolk *yk, yolk_dialog_cb_t update_cb, void *user, uint16_t icon, char *title_fmt, char *text_fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, text_fmt);

	// Enter
	yolk_dialog_enter_std_btn_v(yk, update_cb, YOLK_DIALOG_STD_BTN_NONE, 0, 0, user, icon, title_fmt, text_fmt, ap);

	// Release Args
	va_end(ap);
}

// Enter Dialog with standard action buttons
void yolk_dialog_enter_std_btn(struct yolk *yk, yolk_dialog_cb_t update_cb, uint8_t btn, yolk_dialog_cb_t action_1, yolk_dialog_cb_t action_2, void *user, uint16_t icon, char *title_fmt, char *text_fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, text_fmt);

	// Enter
	yolk_dialog_enter_std_btn_v(yk, update_cb, btn, action_1, action_2, user, icon, title_fmt, text_fmt, ap);

	// Release Args
	va_end(ap);
}

// Enter Dialog with standard action buttons (va_list)
void yolk_dialog_enter_std_btn_v(struct yolk *yk, yolk_dialog_cb_t update_cb, uint8_t btn, yolk_dialog_cb_t action_1, yolk_dialog_cb_t action_2, void *user, uint16_t icon, char *title_fmt, char *text_fmt, va_list ap)
{
	struct yolk_dialog_data *d;
	uint16_t size;

	// Acquire Dialog Data
	d = (struct yolk_dialog_data *)(yk->component_data);
	size = yk->component_data_size - sizeof(struct yolk_dialog_data);

	// Setup Dialog
	d->std_btn = btn;
	d->action_1 = action_1;
	d->action_2 = action_2;
	d->update_cb = update_cb;
	d->user = user;
	d->icon = icon;

	// Set Title
	d->title = (char *)&(yk->component_data[sizeof(struct yolk_dialog_data)]);
	d->title_len = cvpsnprintf(d->title, size, title_fmt, &ap);
	size = size - d->title_len;

	// Set Text
	d->text = (char *)&(yk->component_data[sizeof(struct yolk_dialog_data) + d->title_len]);
	d->text_max_len = size;
	d->text_len = cvpsnprintf(d->text, size, text_fmt, &ap);

	// Invalidate UI Component Area
	yolk_ui_invalidate_component(yk);

	// Set Active Component
	yolk_ui_set_component(yk, (yolk_component_cb_t)yolk_dialog_update, (yolk_component_cb_t)yolk_dialog_draw, 0);
}

// Update Icon
void yolk_dialog_set_icon(struct yolk *yk, uint16_t icon)
{
	struct yolk_dialog_data *d;

	// Acquire Dialog Data & Set Icon
	d = (struct yolk_dialog_data *)(yk->component_data);
	d->icon = icon;

	// Invalidate UI Component Head Area
	yolk_ui_invalidate_component_head(yk);
}

// Update Text
void yolk_dialog_set_text(struct yolk *yk, char *fmt, ...)
{
	va_list ap;
	struct yolk_dialog_data *d;

	// Acquire Args
	va_start(ap, fmt);

	// Acquire Dialog Data & Set Text
	d = (struct yolk_dialog_data *)(yk->component_data);
	d->text_len = cvpsnprintf(d->text, d->text_max_len, fmt, &ap);

	// Release Args
	va_end(ap);

	// Invalidate UI Component Area
	yolk_ui_invalidate_component_body(yk);
}

// Update Dialog
void yolk_dialog_update(struct yolk *yk, struct yolk_dialog_data *data, uint16_t data_size)
{
	// Pass Update to Callback
	if(data->update_cb)																		{ data->update_cb(yk, data->user); }
	else
	{
		// Handle Standard Actions
		if((yolk_in_center(yk) || yolk_in_u1(yk) || yolk_in_right(yk)) && (data->action_1))	{ data->action_1(yk, data->user); }
		if((yolk_in_u2(yk) || yolk_in_left(yk)) && (data->action_2))						{ data->action_2(yk, data->user); }
	}
}

// Draw Dialog
void yolk_dialog_draw(struct yolk *yk, struct yolk_dialog_data *data, uint16_t data_size)
{
	char *btn1;
	char *btn2;

	// Draw Head
	yolk_ui_draw_component_head(yk, data->icon, data->title, data->title_len);

	// Draw Text
	gfx_txt_n(yk->dsp, data->text, data->text_len, yolk_ui_body_x(yk), yolk_ui_body_y(yk), yolk_ui_body_w(yk), yolk_dialog_txt_h(yk), yolk_txt_font(yk), yolk_txt_col(yk));

	// Draw Buttons
	if(data->std_btn == YOLK_DIALOG_STD_BTN_YES_NO)											{ btn1 = "Yes"; btn2 = "No"; }
	else if(data->std_btn == YOLK_DIALOG_STD_BTN_OK_CANCEL)									{ btn1 = "Ok"; btn2 = "Cancel"; }
	else if(data->std_btn == YOLK_DIALOG_STD_BTN_OK)										{ btn1 = "Ok"; btn2 = 0; }
	else																					{ btn1 = 0; btn2 = 0; }
	if(btn1)																				{ gfx_txt(yk->dsp, btn1, yolk_dialog_btn_x_1(yk), yolk_dialog_btn_y(yk), yolk_txt_font(yk)->fw * strlen(btn1), yolk_txt_font(yk)->fh, yolk_txt_font(yk), yolk_ttl_col(yk)); }
	if(btn2)																				{ gfx_txt(yk->dsp, btn2, yolk_dialog_btn_x_2(yk, btn2), yolk_dialog_btn_y(yk), yolk_txt_font(yk)->fw * strlen(btn1), yolk_txt_font(yk)->fh, yolk_txt_font(yk), yolk_ttl_col(yk)); }
}
