/* Dooba SDK
 * Generic GUI Framework
 */

#ifndef	__YOLK_SPINWAIT_H
#define	__YOLK_SPINWAIT_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "../ui.h"
#include "../input.h"
#include "../yolk.h"

// Speed (Interval)
#ifndef	YOLK_SPINWAIT_SPEED
#define	YOLK_SPINWAIT_SPEED																500
#endif

// Layout Shortcuts
#define	yolk_spinwait_stat_y(yk)														yolk_ui_body_y(yk)
#define	yolk_spinwait_stat_h(yk, data)													((((data)->show_progress) && (yolk_pbar_tlst(yk)->th > yolk_spinner_tlst(yk)->th)) ? yolk_pbar_tlst(yk)->th : yolk_spinner_tlst(yk)->th)
#define	yolk_spinwait_stat_baseline(yk, data)											(yolk_spinwait_stat_y(yk) + (yolk_spinwait_stat_h(yk, data) / 2))
#define	yolk_spinwait_spin_x(yk, data)													((data)->show_progress ? yolk_ui_margin(yk) : ((gfx_dsp_get_width((yk)->dsp) / 2) - (yolk_spinner_tlst(yk)->tw / 2)))
#define	yolk_spinwait_spin_y(yk, data)													(yolk_spinwait_stat_baseline(yk, data) - (yolk_spinner_tlst(yk)->th / 2))
#define	yolk_spinwait_pbar_x(yk)														(yolk_spinner_tlst(yk)->tw + (yolk_ui_margin(yk) * 2))
#define	yolk_spinwait_pbar_y(yk, data)													(yolk_spinwait_stat_baseline(yk, data) - (yolk_pbar_tlst(yk)->th / 2))
#define	yolk_spinwait_pbar_w(yk)														(gfx_dsp_get_width((yk)->dsp) - (yolk_spinwait_pbar_x(yk) + yolk_ui_margin(yk)))
#define	yolk_spinwait_text_x(yk)														yolk_ui_margin(yk)
#define	yolk_spinwait_text_y(yk, data)													(yolk_spinwait_stat_y(yk) + yolk_spinwait_stat_h(yk, data) + yolk_ui_margin(yk))
#define	yolk_spinwait_text_w(yk)														yolk_ui_body_w(yk)
#define	yolk_spinwait_text_h(yk, data)													(yolk_ui_body_h(yk) - (yolk_spinwait_stat_h(yk, data) + yolk_ui_margin(yk)))

// Update Callback Type
typedef	void (*yolk_spinwait_update_cb_t)(struct yolk *yk, void *user);

// Spinwait Data Structure
struct yolk_spinwait_data
{
	// Update Callback
	yolk_spinwait_update_cb_t update_cb;

	// User Data
	void *user;

	// Icon
	uint16_t icon;

	// Progress (%)
	uint8_t show_progress;
	uint8_t progress;

	// Spinner Frame
	uint16_t frame;

	// Title
	char *title;
	uint16_t title_len;

	// Text
	char *text;
	uint16_t text_len;
	uint16_t text_size;

	// Counter
	uint16_t counter;
};

// Enter Spinwait
extern void yolk_spinwait_enter(struct yolk *yk, yolk_spinwait_update_cb_t update_cb, void *user, uint16_t icon, uint8_t show_progress, char *title_fmt, char *text_fmt, ...);

// Update Spinwait
extern void yolk_spinwait_update(struct yolk *yk, struct yolk_spinwait_data *data, uint16_t data_size);

// Set Text
extern void yolk_spinwait_set_text(struct yolk *yk, char *fmt, ...);

// Set Progress Visibility
extern void yolk_spinwait_set_progress_visible(struct yolk *yk, uint8_t visible);

// Set Progress
extern void yolk_spinwait_set_progress(struct yolk *yk, uint8_t progress);

// Draw Spinwait
extern void yolk_spinwait_draw(struct yolk *yk, struct yolk_spinwait_data *data, uint16_t data_size);

#endif
